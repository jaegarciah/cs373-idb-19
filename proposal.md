# MusicMap

## Group Info

**IDB Group 19**

**Group Members:** Dewayne Benson, Aneesh Chakka, Jae Garcia-Herrera, Amro Kerkiz, Jasmine Wang


## Project Info

**Project Name:** MusicMap.me

**Project Proposal:** MusicMap.me will be a website that allows users to explore music geographically through artists and concerts in different cities. The site will display information about artists, concerts, cities, and the connections between them. It will not only help music enthusiasts find their favorite artists’ upcoming events, but also encourage them to discover more artists and concerts that they may otherwise not have heard about. Users can also learn more about concerts happening near them or in another city that they may be visiting. For cities that they are interested in visiting for a concert, they can learn more information about the city before making a decision. Ultimately, we hope that MusicMap will inspire users to expand their horizons by exploring new artists, concerts, and cities!

### APIs
* Spotify Web API (for artists): https://developer.spotify.com/documentation/web-api/
* Ticketmaster Discovery API (for concerts): https://developer.ticketmaster.com/products-and-docs/apis/discovery-api/v2/
* RoadGoat Cities API (for cities): https://www.roadgoat.com/business/cities-api

### Models
#### Artists

* **Estimated Instance Count:** 11 million available (but will probably use a bit less)
* **Attributes for fitering and sorting:** name, genres, age, followers, location
* **Media & more attributes:** images, related artists, top albums/tracks + audio preview
* **Connection to other models:** artists play at concerts in different cities & are originally from different cities

#### Concerts (within USA)

* **Estimated Instance Count:** 50k events
* **Attributes for fitering and sorting:** date, location, price ranges, venue, artists
* **Media & more attributes:** location pin, description, images, sale information
* **Connection to other models:** concerts take place in various cities where artists perform

#### Cities (within USA)

* **Estimated Instance Count:** 1000 (more available, so could use more)
* **Attributes for fitering and sorting:** population, location, average rating, safety, budget
* **Media & more attributes:** images, location pin, state, known-for traits
* **Connection to other models:** cities have venues where artists perform at concerts & artists are from certain cities



### Organizational Technique
As of now, we plan to use the same standard organizational technique that was used in past semesters: one page per model with a grid-like view of instances. Later on, we could consider using a map view to display instances.

### Questions
1. What artists are going to have concerts near me?
2. What concerts are there in this city that I’m visiting?
3. Which city should I choose to go see this artist perform?
4. Where and when can I see this artist in concert?
