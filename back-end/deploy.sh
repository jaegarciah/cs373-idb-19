echo "Deploying Backend..."
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 785674758284.dkr.ecr.us-east-1.amazonaws.com/musicmap-backend
docker build -t musicmap-backend .
docker tag musicmap-backend:latest 785674758284.dkr.ecr.us-east-1.amazonaws.com/musicmap-backend:latest
docker push 785674758284.dkr.ecr.us-east-1.amazonaws.com/musicmap-backend:latest
cd aws_deploy
eb deploy
cd ..