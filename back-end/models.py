from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import pymysql

app = Flask(__name__)
CORS(app)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://admin:musicmap19@musicmap-db.czvqbdyghd45.us-east-1.rds.amazonaws.com:3306/musicmap"
db = SQLAlchemy(app)


class City(db.Model):
    __tablename__ = "City"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80))
    short_name = db.Column(db.String(80))
    long_name = db.Column(db.String(80))
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    population = db.Column(db.Integer())
    budget = db.Column(db.Integer())
    safety = db.Column(db.Integer())
    rating = db.Column(db.Float())
    photo = db.Column(db.String(150))
    known_for = db.Column(db.String(150))
    concert_ids = db.Column(db.String(500))
    filters = {"budget", "safety", "population", "rating"}
    sorts = {"name", "population", "rating", "budget", "safety"}


class Concert(db.Model):
    __tablename__ = "Concert"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80))
    city = db.Column(db.String(60))
    state = db.Column(db.String(20))
    artists = db.Column(db.String(500))
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    venue = db.Column(db.String(100))
    venue_addr = db.Column(db.String(100))
    venue_img_url = db.Column(db.String(100))
    date = db.Column(db.String(20))
    time = db.Column(db.String(20))
    timezone = db.Column(db.String(20))
    img_url = db.Column(db.String(150))
    tm_url = db.Column(db.String(150))
    filters = {"timezone"}
    sorts = {"date", "time", "name", "venue"}


class Artist(db.Model):
    __tablename__ = "Artist"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80))
    spotify_id = db.Column(db.String(80))
    spotify_url = db.Column(db.String(150))
    genre = db.Column(db.String(50))
    img_url = db.Column(db.String(150))
    followers = db.Column(db.Integer())
    popularity = db.Column(db.Integer())
    city = db.Column(db.String(50))
    country = db.Column(db.String(50))
    birthday = db.Column(db.String(20))
    gender = db.Column(db.String(10))
    twitter_user = db.Column(db.String(50))
    concert_ids = db.Column(db.String(500))
    filters = {"gender", "genre", "country"}
    sorts = {"name", "birthday", "followers", "popularity"}

