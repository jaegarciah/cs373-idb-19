import requests
from requests.auth import HTTPBasicAuth
import json, os


def get_cities(size):
    apiKey = "1c256b3722a1987d4b94e310e64812d7"
    secretKey = "08020fb0230afee59473aec2ef5a2f44"
    URL = "https://api.roadgoat.com/api/v2/destinations/"

    citySlugs = []
    citiesListJSON = open("city_list.json")
    city_list = json.load(citiesListJSON)
    for city in city_list:
        citySlugs.append(city["slug"])

    print("scraping...")
    cities = []
    counter = 0
    for i in range(0, size):
        try:
            currentCity = citySlugs[i] + "-usa"
            request = requests.get(
                URL + currentCity, auth=HTTPBasicAuth(apiKey, secretKey)
            )
            response = request.text
            city = json.loads(response)
        except IndexError:
            continue

        current_city = {}
        current_city["id"] = counter
        try:
            current_city["name"] = city["data"]["attributes"]["name"]
            name = current_city["name"]
            current_city["state"] = city["data"]["attributes"]["long_name"].split(",")[
                -2
            ]
            current_city["short_name"] = city["data"]["attributes"]["short_name"]
            current_city["long_name"] = city["data"]["attributes"]["long_name"]
            current_city["latitude"] = city["data"]["attributes"]["latitude"]
            current_city["longitude"] = city["data"]["attributes"]["longitude"]
            current_city["population"] = city["data"]["attributes"]["population"]
            current_city["budget"] = city["data"]["attributes"]["budget"][name]["value"]
            current_city["safety"] = city["data"]["attributes"]["safety"][name]["value"]
            current_city["rating"] = city["data"]["attributes"]["average_rating"]
            known_for_list = city["data"]["relationships"]["known_for"]["data"]
            current_city["known_for"] = [item["id"] for item in known_for_list]
            photoID = city["data"]["relationships"]["photos"]["data"][0]["id"]
            included = city["included"]
            for resource in included:
                if resource["id"] == photoID:
                    current_city["photo"] = resource["attributes"]["image"]["full"]
        except KeyError as e:
            print("KeyError", e)
            continue

        cities.append(current_city)
        counter += 1

    create_json_file("./cities.json", cities)
    print("done")


def create_json_file(file_path, data=[]):
    """
    creates a json file with a list at the specified path if it does not exist
    """
    JSON_INDENT = 4
    file_exists = os.path.exists(file_path)
    if not file_exists:
        with open(file_path, "w") as file:
            res = {"cities": data}
            json.dump(res, file, indent=JSON_INDENT)


if __name__ == "__main__":
    get_cities(200)
