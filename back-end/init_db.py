import json
from models import Concert, City, Artist, app, db
from schema import concert_schema
from flask_cors import CORS

CORS(app)


def populate_db_concerts():
    with open("concerts.json") as jsn:
        concert_data = json.load(jsn)
        concert_data = concert_data["concerts"]
        for concerts in concert_data:
            db_row = {
                "id": int(concerts["id"]),
                "name": concerts["name"],
                "city": concerts["city"],
                "state": concerts["state"],
                "artists": concerts["artists"],
                "latitude": float(concerts["latitude"]),
                "longitude": float(concerts["longitude"]),
                "venue": concerts["venue"],
                "venue_addr": concerts["venue_addr"],
                "venue_img_url": concerts["venue_img_url"],
                "date": concerts["date"],
                "time": concerts["time"],
                "timezone": concerts["timezone"],
                "img_url": concerts["img_url"],
                "tm_url": concerts["tm_url"],
            }
            db.session.add(Concert(**db_row))
        db.session.commit()


# returns string containing all concert ids that contain this artists
def search_concerts_artists(name):
    name = "%" + name + "%"
    concerts_query = db.session.query(Concert).filter(Concert.artists.like(name)).all()
    concert_list = concert_schema.dump(concerts_query, many=True)
    concert_ids = ""
    for concert in concert_list:
        concert_ids += str(concert["id"]) + " "
    concert_ids = concert_ids[:-1]
    return concert_ids


# returns string containing all concert ids that are in the given city
def search_concerts_cities(name):
    name = "%" + name + "%"
    concerts_query = db.session.query(Concert).filter(Concert.city.like(name)).all()
    concert_list = concert_schema.dump(concerts_query, many=True)
    concert_ids = ""
    for concert in concert_list:
        concert_ids += str(concert["id"]) + " "
    concert_ids = concert_ids[:-1]
    return concert_ids


def populate_db_artists():
    with open("artist_twitter_users.json") as json_one:
        mapping = json.load(json_one)
        with open("artists.json") as jsn:
            artist_data = json.load(jsn)
            for id, artist in enumerate(artist_data):
                name = artist["name"]
                concert_ids = search_concerts_artists(name)
                db_row = {
                    "id": id + 1,
                    "name": artist["name"],
                    "spotify_id": artist["spotify_id"],
                    "spotify_url": artist["spotify_url"],
                    "genre": artist["genre"],
                    "img_url": artist["img_url"],
                    "followers": artist["followers"]["total"],
                    "popularity": artist["popularity"],
                    "city": artist["city"],
                    "country": artist["country"],
                    "birthday": artist["birthday"],
                    "gender": artist["gender"],
                    "twitter_user": mapping[name],
                    "concert_ids": concert_ids,
                }
                db.session.add(Artist(**db_row))
            db.session.commit()


def populate_db_cities():
    with open("cities.json") as jsn:
        city_data = json.load(jsn)["cities"]
        for city in city_data:
            concert_ids = search_concerts_cities(city["short_name"])
            db_row = {
                "id": int(city["id"]) + 1,
                "name": city["name"],
                "short_name": city["short_name"],
                "long_name": city["long_name"],
                "latitude": city["latitude"],
                "longitude": city["longitude"],
                "population": city["population"],
                "budget": city["budget"],
                "safety": city["safety"],
                "rating": city["rating"],
                "photo": city["photo"],
                "known_for": json.dumps(city["known_for"]),
                "concert_ids": concert_ids,
            }
            db.session.add(City(**db_row))
        db.session.commit()


def reset_db():
    db.session.remove()
    db.drop_all()
    db.create_all()


def init_db():
    reset_db()
    populate_db_concerts()
    populate_db_artists()
    populate_db_cities()


if __name__ == "__main__":
    with app.app_context():
        init_db()
