import requests
import json
import time

api_key = "Or05y2afpguVM5FbKaTH5igU0yO7Z1zi"


def __get_concerts_api(size, page, artist, city):
    # Plan to make this more capable with different sizes searching different pages
    artist_keyword = ""
    city_keyword = ""
    if artist:
        artist = artist.split(" ")
        artist_keyword = "&keyword="
        artist_keyword += artist[0]
        artist.remove(artist[0])
        for part in artist:
            artist_keyword += "%20" + part
    if city:
        city = city.split(" ")
        city_keyword = "&city="
        city_keyword += city[0]
        city.remove(city[0])
        for part in city:
            city_keyword += "%20" + part
    ret = requests.get(
        "https://app.ticketmaster.com/discovery/v2/events?apikey="
        + api_key
        + artist_keyword
        + "&locale=*&size="
        + str(size)
        + "&page="
        + str(page)
        + city_keyword
        + "&countryCode=US&classificationName=music"
    )
    if ret.json().get("_embedded") == None or ret.json()["_embedded"]["events"] == None:
        return []
    concerts_json = ret.json()["_embedded"]["events"]
    return concerts_json


def __get_specific_data(event, id):
    try:
        # What to expect
        concert = {}
        concert["id"] = id
        concert["name"] = event["name"]
        concert["city"] = event["_embedded"]["venues"][0]["city"]["name"]
        concert["state"] = event["_embedded"]["venues"][0].get("state")["name"]

        artists = ""
        for artist in event["_embedded"]["attractions"]:
            artists += artist["name"] + ","
        concert["artists"] = artists[:-1]

        concert["latitude"] = event["_embedded"]["venues"][0]["location"]["latitude"]
        concert["longitude"] = event["_embedded"]["venues"][0]["location"]["longitude"]
        concert["venue"] = event["_embedded"]["venues"][0]["name"]
        concert["venue_addr"] = event["_embedded"]["venues"][0]["address"]["line1"]
        venue_images = event["_embedded"]["venues"][0].get("images")
        for image in venue_images:
            concert["venue_img_url"] = image["url"]
        concert["date"] = event["dates"]["start"]["localDate"]
        concert["time"] = event["dates"]["start"].get("localTime")
        if concert["time"] == None:
            return (None, None)
        concert["timezone"] = event["dates"].get("timezone")
        concert["img_url"] = event["images"][0]["url"]
        concert["tm_url"] = event["url"]
        return concert, event["id"]
    except:
        return (None, None)


def get_concerts(size):
    all_concerts = []
    counter = 1
    total_elem = size
    artist_json = json.load(open("artists.json"))
    city_json = json.load(open("cities.json"))["cities"]
    concert_set = set()
    searching_artists = True
    index = 0
    while total_elem > 0:
        city_name = None
        artist_name = None
        if searching_artists:
            try:
                artist_name = artist_json[index]["name"]
            except:
                searching_artists = False
                index = 0
        else:
            try:
                city_name = city_json[index]["short_name"]
            except:
                break
        concerts_json = __get_concerts_api(30, 0, artist_name, city_name)
        index += 1
        for event in concerts_json:
            if total_elem == 0:
                break
            concert_data, cur_id = __get_specific_data(event, counter)

            if concert_data != None:
                if cur_id in concert_set:
                    continue
                counter += 1
                total_elem -= 1
                all_concerts.append(concert_data)
                concert_set.add(cur_id)

    res = {"concerts": all_concerts}
    return res


if __name__ == "__main__":
    res = get_concerts(3000)
    with open("./concerts.json", "w") as file:
        json.dump(res, file, indent=4)
