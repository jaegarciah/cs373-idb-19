import unittest
import requests

domain = "https://api.musicmap.me"


class UnitTests(unittest.TestCase):
    def test_hello_world(self):
        resp = requests.get(domain)
        self.assertEqual(resp.text, "Hello World")

    def test_concerts(self):
        resp = requests.get(domain + "/concerts")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        rjson = rjson["concerts"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_concerts_pagination(self):
        resp = requests.get(domain + "/concerts?page=0&perPage=10")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        rjson = rjson["concerts"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_concerts_filter(self):
        resp = requests.get(domain + "/concerts?timezone=America/Chicago")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        rjson = rjson["concerts"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_concerts_sort(self):
        resp = requests.get(domain + "/concerts?sort=name_asc")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        rjson = rjson["concerts"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_concerts_search(self):
        resp = requests.get(domain + "/concerts?search=P!NK")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        rjson = rjson["concerts"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_concerts_id(self):
        resp = requests.get(domain + "/concerts/4")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concert", rjson)
        self.assertIn("artists", rjson)
        self.assertIn("city", rjson)
        indexed_json = rjson["concert"]
        self.assertNotEqual(indexed_json, None)
        self.assertNotEqual(indexed_json[0], None)
        indexed_json = rjson["artists"]
        self.assertNotEqual(indexed_json, None)
        indexed_json = rjson["city"]
        self.assertNotEqual(indexed_json, None)

    def test_artists(self):
        resp = requests.get(domain + "/artists")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("artists", rjson)
        rjson = rjson["artists"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_artists_pagination(self):
        resp = requests.get(domain + "/artists?page=1&perPage=7")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("artists", rjson)
        rjson = rjson["artists"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_artists_filter(self):
        resp = requests.get(domain + "/artists?gender=male")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("artists", rjson)
        rjson = rjson["artists"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_artists_sort(self):
        resp = requests.get(domain + "/artists?sort=birthday_desc")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("artists", rjson)
        rjson = rjson["artists"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_artists_search(self):
        resp = requests.get(domain + "/artists?search=Drake")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("artists", rjson)
        rjson = rjson["artists"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_artists_id(self):
        resp = requests.get(domain + "/artists/14")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        self.assertIn("artist", rjson)
        self.assertIn("cities", rjson)
        self.assertIn("homecity", rjson)
        indexed_json = rjson["concerts"]
        self.assertNotEqual(indexed_json, None)
        indexed_json = rjson["artist"]
        self.assertNotEqual(indexed_json, None)
        self.assertNotEqual(indexed_json[0], None)
        indexed_json = rjson["cities"]
        self.assertNotEqual(indexed_json, None)
        indexed_json = rjson["homecity"]
        self.assertNotEqual(indexed_json, None)

    def test_cities(self):
        resp = requests.get(domain + "/cities")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("cities", rjson)
        rjson = rjson["cities"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_cities_pagination(self):
        resp = requests.get(domain + "/cities?page=4&perPage=10")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("cities", rjson)
        rjson = rjson["cities"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_cities_filter(self):
        resp = requests.get(domain + "/cities?safety=2-4")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("cities", rjson)
        rjson = rjson["cities"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_cities_sort(self):
        resp = requests.get(domain + "/cities?sort=population_asc")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("cities", rjson)
        rjson = rjson["cities"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_cities_search(self):
        resp = requests.get(domain + "/cities?search=Austin")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("cities", rjson)
        rjson = rjson["cities"]
        self.assertNotEqual(rjson, None)
        self.assertNotEqual(rjson[0], None)

    def test_cities_id(self):
        resp = requests.get(domain + "/cities/23")
        self.assertEqual(resp.status_code, 200)
        rjson = resp.json()
        self.assertIn("concerts", rjson)
        self.assertIn("artists", rjson)
        self.assertIn("city", rjson)
        indexed_json = rjson["concerts"]
        self.assertNotEqual(indexed_json, None)
        indexed_json = rjson["artists"]
        self.assertNotEqual(indexed_json, None)
        indexed_json = rjson["city"]
        self.assertNotEqual(indexed_json, None)
        self.assertNotEqual(indexed_json[0], None)


if __name__ == "__main__":
    unittest.main()
