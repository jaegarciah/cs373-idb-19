from flask_marshmallow import Marshmallow
from models import Concert, City, Artist
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

ma = Marshmallow()


class CitySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = City


class ConcertSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Concert


class ArtistSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Artist


city_schema = CitySchema()
concert_schema = ConcertSchema()
artist_schema = ArtistSchema()
