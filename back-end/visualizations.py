import requests
import json

# Get data for visualizing the amount of artists per genre
def get_genre_artist_count():
    genre_to_artists = {}
    artists_raw = requests.get("https://api.musicmap.me/artists")
    artists = artists_raw.json()["artists"]
    for artist in artists:
        genre_to_artists[artist["genre"]] = genre_to_artists.get(artist["genre"], 0) + 1
    with open("./artists_visualization.json", "w") as file:
        json.dump(genre_to_artists, file, indent=4)

# Get data for visualizing the amount of concerts per state
def get_state_concert_count():
    state_to_concert = {}
    concerts_raw = requests.get("https://api.musicmap.me/concerts")
    concerts = concerts_raw.json()["concerts"]
    for concert in concerts:
        state_to_concert[concert["state"]] = state_to_concert.get(concert["state"], 0) + 1
    with open("./concerts_visualization.json", "w") as file:
        json.dump(state_to_concert, file, indent=4)

# Get data for visualizing the budget and safety values per city
def get_city_budget_safety():
    city_to_budget_safety = {}
    cities_raw = requests.get("https://api.musicmap.me/cities")
    cities = cities_raw.json()["cities"]
    for city in cities:
        city_to_budget_safety[city["name"]] = {"budget" : city["budget"], "safety" : city["safety"]}
    with open("./cities_visualization.json", "w") as file:
        json.dump(city_to_budget_safety, file, indent=4)

# Get data for visualizing the wins per team
def get_wins_per_team():
    wins_to_team = {}
    championships_raw = requests.get("https://api.marchmadnessmayhem.me/championships")
    championships = championships_raw.json()["data"]
    for championship in championships:
        wins_to_team[championship["champion"]] = wins_to_team.get(championship["champion"], 0) + 1
    with open("./provider_visualizations/championships_visualization.json", "w") as file:
        json.dump(wins_to_team, file, indent=4)

# Get data for visualizing the teams per state
def get_teams_per_state():
    state_to_teams = {}
    teams_raw = requests.get("https://api.marchmadnessmayhem.me/teams")
    teams = teams_raw.json()["data"]
    for team in teams:
        state_to_teams[team["stateLocation"]] = state_to_teams.get(team["stateLocation"], 0) + 1
    with open("./provider_visualizations/teams_visualization.json", "w") as file:
        json.dump(state_to_teams, file, indent=4)

# Get data for visualizing the games and points per each player
def get_players_games_points():
    player_to_games_points = {}
    players_raw = requests.get("https://api.marchmadnessmayhem.me/players")
    players = players_raw.json()["data"]
    for player in players:
        player_to_games_points[player["name"]] = {"games" : player["games"], "points" : player["points"]}
    with open("./provider_visualizations/players_visualization.json", "w") as file:
        json.dump(player_to_games_points, file, indent=4)

get_wins_per_team()
get_teams_per_state()
get_players_games_points()