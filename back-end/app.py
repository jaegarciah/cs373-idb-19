from flask import Flask, jsonify, request
from models import app, db, Concert, Artist, City
from flask_cors import CORS
from concerts_api import get_concerts
import pymysql
import requests
import json
from sqlalchemy import desc
from sqlalchemy.sql import text, or_, not_
from schema import concert_schema, artist_schema, city_schema

CORS(app)

# backend is hosted on a separate ec2 instance from the frontend,
# so no need to specify "/api" in the route - it is handled elsewhere
@app.route("/")
def hello():
    return "Hello World"


# get all concerts
@app.route("/concerts", methods=["GET"])
def concerts_request():

    # make query for search/filter/sort
    concerts_query = db.session.query(Concert)

    #Pages & sorting
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    sort = request.args.get("sort")

    #search
    search = request.args.get("search")
    if search != None:
        search_terms = search.split()
        clauses = [Concert.city.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Concert.state.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Concert.name.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Concert.venue.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Concert.artists.like("%{0}%".format(k)) for k in search_terms]
        concerts_query = concerts_query.filter(or_(*clauses))

    for filter in Concert.filters:
        arg = request.args.get(filter)
        if arg != None:
            concerts_query = concerts_query.filter(getattr(Concert, filter) == arg)

    #implement sorting
    if sort != None:
        sort_params = sort.split("_")
        if sort_params[0] in Concert.sorts:
            sort_attr = getattr(Concert, sort_params[0])
            asc = sort_params[1] == "asc"
            concerts_query = concerts_query.order_by(sort_attr if asc else desc(sort_attr)) 

    #pagination
    if page != None:
        concerts_query = concerts_query.paginate(
            page=page, per_page=perPage, error_out=False
        ).items  # Should be based off of routing paramets for how much per page
    else:
        concerts_query = concerts_query.all()
    result = concert_schema.dump(concerts_query, many=True)
    for concert in result:
        concert["artists"] = concert["artists"].split(",")
    mapping = {"concerts": result}

    return jsonify(mapping)


# get concert via ID
@app.route("/concerts/<int:given_id>", methods=["GET"])
def concerts_request_id(given_id):
    concerts_query = db.session.query(Concert).filter_by(id=given_id)
    result = concert_schema.dump(concerts_query, many=True)
    result[0]["artists"] = result[0]["artists"].split(",")
    artist_list = []
    for artist in result[0]["artists"]:
        artist_list += get_artists_from_concert(artist)
    mapping = {
        "concert": result,
        "artists": artist_list,
        "city": get_cities_from_concert(result[0]["city"]),
    }
    return jsonify(mapping)


# get all artists
@app.route("/artists", methods=["GET"])
def artists_request():
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    sort = request.args.get("sort")
    search = request.args.get("search") 
    artists_query = db.session.query(Artist)

    if search != None:
        search_terms = search.split()
        clauses = [Artist.name.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Artist.city.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Artist.country.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Artist.genre.like("%{0}%".format(k)) for k in search_terms]
        clauses += [Artist.twitter_user.like("%{0}%".format(k)) for k in search_terms]
        artists_query = artists_query.filter(or_(*clauses))

    for filter in Artist.filters:
        arg = request.args.get(filter)
        if arg != None:
            artists_query = artists_query.filter(getattr(Artist, filter).like(f"%{arg}%"))

    #implement sorting
    if sort != None:
        sort_params = sort.split("_")
        if sort_params[0] in Artist.sorts:
            sort_attr = getattr(Artist, sort_params[0])
            asc = sort_params[1] == "asc"
            artists_query = artists_query.order_by(sort_attr if asc else desc(sort_attr)) 

    if page != None:
        artists_query = artists_query.paginate(
            page=page, per_page=perPage, error_out=False
        ).items  # Should be based off of routing paramets for how much per page
    else:
        artists_query = artists_query.all()
    result = artist_schema.dump(artists_query, many=True)
    for artist in result:
        artist["concert_ids"] = artist["concert_ids"].split(" ")
    mapping = {"artists": result}

    return jsonify(mapping)


# get artist via ID
@app.route("/artists/<int:given_id>", methods=["GET"])
def artists_request_id(given_id):
    artists_query = db.session.query(Artist).filter_by(id=given_id)
    result = artist_schema.dump(artists_query, many=True)
    name = result[0]["name"]
    concerts = get_concerts_from_artist(name)  # get concerts
    cities = get_cities_from_artist(name)
    homecity = get_homecity_from_artist(name)
    mapping = {
        "artist": result,
        "concerts": concerts,
        "cities": cities,
        "homecity": homecity,
    }

    return jsonify(mapping)


# get all cities
@app.route("/cities", methods=["GET"])
def cities_request():
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    sort = request.args.get("sort")
    search = request.args.get("search")
    cities_query = db.session.query(City)

    if search != None:
        search_terms = search.split()
        clauses = [City.long_name.like("%{0}%".format(k)) for k in search_terms]
        clauses += [City.budget.like("%{0}%".format(k)) for k in search_terms]
        clauses += [City.population.like("%{0}%".format(k)) for k in search_terms]
        clauses += [City.rating.like("%{0}%".format(k)) for k in search_terms]
        clauses += [City.safety.like("%{0}%".format(k)) for k in search_terms]
        cities_query = cities_query.filter(or_(*clauses))

    for filter in City.filters:
        arg = request.args.get(filter)
        if arg != None:
            min, max = arg.split("-")
            cities_query = cities_query.filter(min <= getattr(City, filter), getattr(City, filter) <= max)

    if sort != None:
        sort_params = sort.split("_")
        if sort_params[0] in City.sorts:
            sort_attr = getattr(City, sort_params[0])
            asc = sort_params[1] == "asc"
            cities_query = cities_query.order_by(sort_attr if asc else desc(sort_attr))

    if page != None:
        cities_query = cities_query.paginate(
            page=page, per_page=perPage, error_out=False
        ).items  # Should be based off of routing paramets for how much per page
    else:
        cities_query = cities_query.all()
    result = city_schema.dump(cities_query, many=True)
    for city in result:
        city["concert_ids"] = city["concert_ids"].split(" ")
    mapping = {"cities": result}

    return jsonify(mapping)


# get city via ID
@app.route("/cities/<int:given_id>", methods=["GET"])
def cities_request_id(given_id):
    cities_query = db.session.query(City).filter_by(id=given_id)
    result = city_schema.dump(cities_query, many=True)
    name = result[0]["short_name"]
    concerts = get_concerts_from_city(name)
    artists = get_artists_from_city(name)
    mapping = {"city": result, "concerts": concerts, "artists": artists}
    return jsonify(mapping)


# get concerts which feature given artist
def get_concerts_from_artist(name):
    name = "%" + name + "%"
    concerts_query = db.session.query(Concert).filter(Concert.artists.like(name)).all()
    concert_list = concert_schema.dump(concerts_query, many=True)
    for concert in concert_list:
        concert["artists"] = concert["artists"].split(",")
    return concert_list


# get concerts which take place at given city
def get_concerts_from_city(name):
    name = "%" + name + "%"
    concerts_query = db.session.query(Concert).filter(Concert.city.like(name)).all()
    concert_list = concert_schema.dump(concerts_query, many=True)
    for concert in concert_list:
        concert["artists"] = concert["artists"].split(",")
    return concert_list


# get artists who are from given city
def get_artists_from_homecity(name):
    name = "%" + name + "%"
    artists_query = db.session.query(Artist).filter(Artist.city.like(name)).all()
    artists_list = artist_schema.dump(artists_query, many=True)
    for artist in artists_list:
        artist["concert_ids"] = artist["concert_ids"].split(" ")
    return artists_list


# get city that artist is from
def get_homecity_from_artist(name):
    name = "%" + name + "%"
    cities_query = db.session.query(City).filter(City.short_name.like(name)).all()
    cities_list = city_schema.dump(cities_query, many=True)
    for city in cities_list:
        city["concert_ids"] = city["concert_ids"].split(" ")
    return cities_list  # will either be empty or have 1 city


# get artists who are featured at given concert
def get_artists_from_concert(name):
    name = "%" + name + "%"
    artists_query = db.session.query(Artist).filter(Artist.name.like(name)).all()
    artists_list = artist_schema.dump(artists_query, many=True)
    for artist in artists_list:
        artist["concert_ids"] = artist["concert_ids"].split(" ")
    return artists_list


# get artists who are featured at given concert
def get_cities_from_concert(name):
    name = "%" + name + "%"
    cities_query = db.session.query(City).filter(City.short_name.like(name)).all()
    cities_list = city_schema.dump(cities_query, many=True)
    for city in cities_list:
        city["concert_ids"] = city["concert_ids"].split(" ")
    return cities_list


# get artists who are performing at given city
def get_artists_from_city(name):
    name = "%" + name + "%"
    concerts_query = db.session.query(Concert).filter(Concert.city.like(name)).all()
    concert_list = concert_schema.dump(concerts_query, many=True)
    artist_list = []
    seen = set()
    for concert in concert_list:
        artists = concert["artists"].split(",")
        for artist in artists:
            if artist not in seen:
                name = "%" + artist + "%"
                artists_query = (
                    db.session.query(Artist).filter(Artist.name.like(name)).all()
                )
                cur_artists_list = artist_schema.dump(artists_query, many=True)
                if len(cur_artists_list) > 0:
                    artist_list.append(cur_artists_list[0])
                seen.add(artist)
    for artist in artist_list:
        artist["concert_ids"] = artist["concert_ids"].split(" ")
    return artist_list


# get cities which are being performed at by given artist
def get_cities_from_artist(name):
    name = "%" + name + "%"
    concerts_query = db.session.query(Concert).filter(Concert.artists.like(name)).all()
    concert_list = concert_schema.dump(concerts_query, many=True)
    city_list = []
    seen = set()
    for concert in concert_list:
        city = concert["city"]
        if city not in seen:
            name = "%" + city + "%"
            cities_query = (
                db.session.query(City).filter(City.short_name.like(name)).all()
            )
            cur_cities_list = city_schema.dump(cities_query, many=True)
            if len(cur_cities_list) > 0:
                city_list.append(cur_cities_list[0])
            seen.add(city)
    for city in city_list:
        city["concert_ids"] = city["concert_ids"].split(" ")
    return city_list
