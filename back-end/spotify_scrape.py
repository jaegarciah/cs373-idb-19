import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import musicbrainzngs
import json, os, time

"""
reference: https://medium.com/@maxtingle/getting-started-with-spotifys-api-spotipy-197c3dc6353b
"""


def get_artists():

    print("\nscraping...")

    # must setup environment variables using export
    # SPOTIPY_CLIENT_ID and SPOTIPY_CLIENT_SECRET
    sp = spotipy.Spotify(auth_manager=SpotifyClientCredentials())
    musicbrainzngs.set_useragent("MusicMap", "v0", "musicmap.me")

    artists = []
    count = 0
    seen = set()

    for i in range(15):
        results = sp.search(
            q="year:2023", type="artist", market="US", offset=len(seen), limit=50
        )
        for artist in results["artists"]["items"]:
            # ignore duplicates & only want artists w/ pictures & genre
            name = artist["name"]
            if (
                name not in seen
                and len(artist["images"]) > 0
                and len(artist["genres"]) > 0
            ):
                result = musicbrainzngs.search_artists(artist=name, limit=1)
                if (
                    len(result["artist-list"]) > 0
                ):  # only want artists w/ musicbrainz info
                    count += 1
                    info = result["artist-list"][0]
                    if (
                        "begin-area" in info
                        and "country" in info
                        and "begin" in info["life-span"]
                    ):  # require some fields
                        # set artist info
                        artist_info = {}
                        artist_info["id"] = count
                        artist_info["name"] = name
                        artist_info["spotify_id"] = artist["id"]
                        artist_info["spotify_url"] = artist["external_urls"]["spotify"]
                        artist_info["genre"] = artist["genres"][0]
                        artist_info["img_url"] = artist["images"][0]["url"]
                        artist_info["followers"] = artist["followers"]
                        artist_info["popularity"] = artist["popularity"]
                        artist_info["city"] = info["begin-area"]["name"]
                        artist_info["country"] = info["country"]
                        artist_info["birthday"] = info["life-span"]["begin"]
                        artist_info["gender"] = (
                            info["gender"] if "gender" in info else "NA"
                        )
                        artists.append(artist_info)
                        seen.add(name)
        print("cycle", i)
        time.sleep(10)  # to avoid rate limit issues

    print("got", count, "artists")
    create_json_file("./artists.json", artists)
    print("done")


def create_json_file(file_path, data=[]):
    """
    creates a json file with a list at the specified path if it does not exist
    """
    JSON_INDENT = 4
    file_exists = os.path.exists(file_path)
    if not file_exists:
        with open(file_path, "w") as file:
            json.dump(data, file, indent=JSON_INDENT)


if __name__ == "__main__":
    get_artists()
