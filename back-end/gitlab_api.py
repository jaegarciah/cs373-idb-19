"""
MusicMap.me version
This provides an API to provide Gitlab information.
Internally, it relies on the Gitlab API.
https://docs.gitlab.com/ee/

"""
import requests

def get_commits():
    # Call to retrieve commits
    url = "https://gitlab.com/jaegarciah/cs373-idb-19/-/commits/main"
    response = requests.get(url)
    return response.json()


def get_issues():
    # Call to retrieve commits
    url = "https://gitlab.com/jaegarciah/cs373-idb-19/-/issues"
    response = requests.get(url)
    return response.json()


def test():
    print(get_commits())
    print(get_issues())

if __name__ == "__main__":
    test()
