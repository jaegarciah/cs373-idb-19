# get git config
config:
	git config -l

# get git log
MusicMap.log.txt:
	git log > MusicMap.log.txt

clean:
	rm -f *.tmp

# run docker for front-end (runs website on local 3000 port)
frontend-docker:
	docker run -d -p 3000:3000 musicmap-frontend

# build front-end docker iamge
build-frontend:
	docker build -t musicmap-frontend front-end/

# run docker for back-end
backend-docker:
	docker run --rm -it -p 5000:5000 musicmap-frontend

# build front-end docker image
build-backend:
	docker build -t musicmap-backend back-end/

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# pull files from repo
pull:
	make clean
	@echo
	git pull
	git status

# auto format the code
format:
	black ./back-end/*.py

all: