import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import ActionChains

URL = "https://www.musicmap.me/"
postmanURL = "https://documenter.getpostman.com/view/25859804/2s93CEvGRq"

class Test(unittest.TestCase):

    @classmethod
    def setUpClass(self) -> None:
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1440, 900")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        self.driver = webdriver.Chrome(options = options, service = Service(ChromeDriverManager().install()))
        self.driver.get(URL)
        #self.driver.maximize_window()
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        
    @classmethod
    def ensureCanClick(self, elementCSS):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, elementCSS))
        )
        return WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, elementCSS))
        )
    
    # Test 1: test whether tab title is MusicMap
    def testTitle(self):
        title = self.driver.title
        self.assertEqual(title, "MusicMap")
    
    # Test 2: test whether About button on navbar leads you to About page
    def testAboutNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "About")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "about/")

    
    # Test 3: test whether Home button leads you to Home page
    def testHomeNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Home")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL)
    
    # Test 4: test whether Artist button leads you to Artists search page
    def testArtistNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Artists")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "artists/")
    
    # Test 6: test clicking on button to go to artist instance page
    def testArtistDetailPage(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Artists")))
        element = self.driver.find_elements(By.LINK_TEXT, "Artists")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[2]/div/div[2]/div/div[1]/div/div/div/a/button")))
        element = self.driver.find_elements(By.XPATH, "/html/body/div/div/div[2]/div/div[2]/div/div[1]/div/div/div/a/button")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        newURL = self.driver.current_url
        self.assertIn("artistdetail", newURL)

    # Test 7: test whether Concert button leads you to Concert search page
    def testConcertNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Concerts")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "concerts/")
    
    # Test 8: test clicking on button to go to concert instance page
    def testConcertDetailPage(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Concerts")))
        element = self.driver.find_elements(By.LINK_TEXT, "Concerts")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/div[2]/div/div[1]/div/div/div/a/button')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/div[2]/div/div[1]/div/div/div/a/button')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        newURL = self.driver.current_url
        self.assertIn("concertdetail", newURL)
    
    # Test 9: test whether City button leads you to concert search page
    def testCityNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Cities")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "cities/")

    
    # Test 10: test clicking on button to go to city instance page
    def testCityDetailPage(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Cities")))
        element = self.driver.find_elements(By.LINK_TEXT, "Cities")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/div[2]/div/div[1]/div/div/div/a/button')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/div[2]/div/div[1]/div/div/div/a/button')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        newURL = self.driver.current_url
        self.assertIn("citydetail", newURL)
        
    # Test 11: test clicking on button to go to about page
    def testAboutPage(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "About")))
        element = self.driver.find_elements(By.LINK_TEXT, "About")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        newURL = self.driver.current_url
        self.assertIn("about", newURL)
    
    # Test 12: test whether Postman documentation is properly linked
    def testPostmanLink(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "About")))
        element = self.driver.find_elements(By.LINK_TEXT, "About")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Postman Documentation")))
        element = self.driver.find_elements(By.LINK_TEXT, "Postman Documentation")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, postmanURL)
        
    # Test 13: test whether navigation bar button appears when squishing window
    def testNavBarSquish(self):
        try:
            self.driver.set_window_size(400, 400)
            WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, "navbar-toggler-icon")))
            element = self.driver.find_elements(By.CLASS_NAME, "navbar-toggler-icon")[0]
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
            self.driver.execute_script("arguments[0].click();", element)
            self.driver.set_window_size(1920, 1080)
            self.assertEqual(1, 1)
            
        except Exception:
            self.assertEqual(1, 2)
        
    # Test 14: test whether carousel pictures on home page operates properly
    def testHomePics(self):
        try:
            WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Home")))
            element = self.driver.find_elements(By.LINK_TEXT, "Home")[0]
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
            self.driver.execute_script("arguments[0].click();", element)
            element = self.driver.find_elements(By.CLASS_NAME, "carousel-control-next-icon")[0]
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
            self.driver.execute_script("arguments[0].click();", element)
            self.assertEqual(1, 1)
            
        except Exception:
            self.assertEqual(1, 2)
        
        # Test 15: test clicking on gender button for Artists
    def testArtistGenderButton(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Artists")))
        element = self.driver.find_elements(By.LINK_TEXT, "Artists")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click gender button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/button")))
        element = self.driver.find_elements(By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/button")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click Female
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]")))
        element = self.driver.find_elements(By.XPATH, "/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        self.assertEqual(1, 1)

        
   # Test 16: test clicking on button to go to concert instance page
    def testConcertTimezoneButton(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Concerts")))
        element = self.driver.find_elements(By.LINK_TEXT, "Concerts")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click Timezone button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/button')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/button')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click America/Chicago
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        #/html/body/div/div/div[2]/div/form/div[1]/div/div/a[2]
        
        self.assertEqual(1, 1)
        
    # Test 17: test clicking on button to go to city instance page
    def testCitySortButton(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Cities")))
        element = self.driver.find_elements(By.LINK_TEXT, "Cities")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click Sort button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form[2]/div[1]/button')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form[2]/div[1]/button')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click Name
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form[2]/div[1]/div/div/a[2]')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form[2]/div[1]/div/div/a[2]')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        self.assertEqual(1, 1)

    # Test 18: test clicking on button to go to city instance page
    def testCitySlider(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.LINK_TEXT, "Cities")))
        element = self.driver.find_elements(By.LINK_TEXT, "Cities")[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        # Find + click Sort button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[2]/div/form[1]/span[1]/span[3]')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[2]/div/form[1]/span[1]/span[3]')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        move = ActionChains(self.driver)
        move.click_and_hold(element).move_by_offset(1,0).release().perform()
        self.assertEqual(1, 1)

    # Test 19: test clicking on button to go to city instance page
    def testSearch(self):
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[1]/nav/div/div/div[2]/form/input')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[1]/nav/div/div/div[2]/form/input')[0]
        
        # Type 'taylor'
        element.send_keys('taylor')

        # Find + click Search button
        WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div/div/div[1]/nav/div/div/div[2]/form/button')))
        element = self.driver.find_elements(By.XPATH, '/html/body/div/div/div[1]/nav/div/div/div[2]/form/button')[0]
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        self.driver.execute_script("arguments[0].click();", element)
        
        newURL = self.driver.current_url
        self.assertIn("search/taylor", newURL)

   
    

if __name__ == '__main__':
    unittest.main()



