import { render, screen, cleanup } from '@testing-library/react';
import Home from '../pages/Home'
import Concerts from '../pages/Concerts'
import Artists from '../pages/Artists'
import ArtistDetail from '../pages/ArtistDetail'
import Cities from '../pages/Cities'
import Search from '../pages/Search'
import About from '../pages/About'
import renderer from 'react-test-renderer';
import APICard from '../components/APICard';
import DeveloperCard from '../components/DeveloperCard';
import CategoryCard from '../components/CategoryCard';
import ArtistCard from '../components/ArtistCard';
import CityCard from '../components/CityCard';
import ConcertCard from '../components/ConcertCard';
import CityDetail from '../pages/CityDetail';
import NavBar from '../components/NavBar';
import RangeSlider from '../components/RangeSlider';
import FilterDropdown from '../components/FilterDropdown';
import { ConcertDetail } from '../pages/ConcertDetail';
import { BrowserRouter } from 'react-router-dom';

describe('Splash pages', () => {
    test('Home', () => {
        const tree = <BrowserRouter>renderer.create(<Home />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Artists', () => {
        const tree = <BrowserRouter>renderer.create(<Artists />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Cities', () => {
        const tree = <BrowserRouter>renderer.create(<Cities />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('Concerts', () => {
        const tree = <BrowserRouter>renderer.create(<Concerts />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

    test('About', () => {
        const tree = <BrowserRouter>renderer.create(<About />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    })

})

describe('Page structures', () => {

    test('API Card', () => {
        const tree = <BrowserRouter>renderer.create(<APICard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Concert Card', () => {
        const tree = <BrowserRouter>renderer.create(<ConcertCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('City Card', () => {
        const tree = <BrowserRouter>renderer.create(<CityCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Artist Card', () => {
        const tree = <BrowserRouter>renderer.create(<ArtistCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Developer Card', () => {
        const tree = <BrowserRouter>renderer.create(<DeveloperCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Category Card', () => {
        const tree = <BrowserRouter>renderer.create(<CategoryCard />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Navigation Bar', () => {
        const tree = <BrowserRouter>renderer.create(<NavBar />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

})

describe('Searching, Sorting and Filtering', () => {

    test('Search Page', () => {
        const tree = <BrowserRouter>renderer.create(<Search />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });
    
    test('Range Slider', () => {
        const tree = <BrowserRouter>renderer.create(<RangeSlider />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Range Slider (Initialized)', () => {

        const handleRatingFilter = (value) => {
            setRating(value)
        }

        const props = {
            min: 0,
            max: 10,
            discrete: null,
            onChange: handleRatingFilter,
          };
        
        const tree = <BrowserRouter>renderer.create(<RangeSlider props = {props}/>).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

    test('Filter Dropdown ', () => {

        const tree = <BrowserRouter>renderer.create(<FilterDropdown/>).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()

    });


    test('Filter Dropdown (Initialized)', () => {

        const handleSortFilter = (value) => {
            setSort(value.toLowerCase().replace(" ", "_"))
        }

        const items = [
            "Sort",
            "Name",
            "Population",
            "Rating",
            "Budget",
            "Safety"
        ]

        const props = {
            title: "Sort",
            items: items,
            scroll: null,
            onChange: handleSortFilter,
          };

        const tree = <BrowserRouter>renderer.create(<FilterDropdown props = {props} />).toJSON()</BrowserRouter>
        expect(tree).toMatchSnapshot()
    });

})



