import React from "react";
import concertScene from './assets/images/concertScene.jpg'

export const HeaderImg = (props: {title: string; subTitle: string}) => {
    return (
        <section>
            <div style={{ backgroundImage: `url(${concertScene})`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                backgroundSize: 'cover'}}>

                <div className="container" style={{minHeight: '350px'}}>
                    <div className="text-center justify-content-center align-self-center">
                        <h1 className="pt-5 pb-3">{props.title}</h1>
                        <h5>{props.subTitle}</h5>
                    </div>
                </div>
            </div>
        </section>
    )
}
