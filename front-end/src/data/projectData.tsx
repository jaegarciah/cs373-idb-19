import spotifyLogo from '../assets/logos/spotifyLogo.png'
import roadgoatLogo from '../assets/logos/roadgoatLogo.png'
import ticketLogo from '../assets/logos/ticketmasterLogo.png'
import microsoftLogo from '../assets/logos/microsoftTeamsLogo.jpeg'
import postmanLogo from '../assets/logos/postmanLogo.png'
import reactLogo from '../assets/logos/reactLogo.png'
import reactBootstrap from '../assets/logos/reactBootstrap.jpg'
import vsCode from '../assets/logos/vsLogo.jpeg'
import gitlab from '../assets/logos/gitlabLogo.png'
import aws from '../assets/logos/awsLogo.jpeg'
import namecheap from '../assets/logos/namecheapLogo.png'

const apiData = [
    {
        name: "Spotify Web API",
        image: spotifyLogo,
        description: "Used to obtain artist details and display music player with songs",
        link: "https://developer.spotify.com/documentation/web-api/",

    },
    {
        name: "Ticketmaster Discovery API",
        image: ticketLogo,
        description: "Used to obtain information about concerts",
        link: "https://developer.ticketmaster.com/products-and-docs/apis/discovery-api/v2/",

    },
    {
        name: "RoadGoat Cities API",
        image: roadgoatLogo,
        description: "Used to obtain statistics about cities, such as population and rating",
        link: "https://www.roadgoat.com/business/cities-api",

    }
];

const toolsData = [
    {
        name: "React",
        image: reactLogo,
        description: "Used in MusicMap's front-end development",
        link: "https://reactjs.org/",

    },
    {
        name: "React-Bootstrap",
        image: reactBootstrap,
        description: "Used to style pages with CSS",
        link: "https://reactjs.org/",

    },
    {
        name: "Postman",
        image: postmanLogo,
        description: "Used to document the MusicMaps API",
        link: "https://postman.com/",

    },
    {
        name: "Namecheap",
        image: namecheap,
        description: "Used to obtain MusicMap.me domain",
        link: "https://reactjs.org/",

    },
    {
        name: "Amazon Web Services",
        image: aws,
        description: "Used to host MusicMap.me with Amplify",
        link: "https://aws.amazon.com",

    },
    {
        name: "Visual Studio Code",
        image: vsCode,
        description: "Used to collaboratively program in live-time",
        link: "https://reactjs.org/",

    },
    {
        name: "GitLab",
        image: gitlab,
        description: "Used to store code in repository",
        link: "https://reactjs.org/",

    },
    {
        name: "Microsoft Teams",
        image: microsoftLogo,
        description: "Used for holding virtual meetings",
        link: "https://www.microsoft.com/en-us/microsoft-teams/group-chat-software",

    }
];

export { apiData, toolsData };