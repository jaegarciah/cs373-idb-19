import Amro from '../assets/teamPics/amro.jpg'
import Dewayne from '../assets/teamPics/dewayne.jpg'
import Jasmine from '../assets/teamPics/jasmine.jpg'
import Aneesh from '../assets/teamPics/aneesh.jpg'
import Jae from '../assets/teamPics/jae.jpg'

export const team_list = [
    {
        name: "Amro Kerkiz",
        image: Amro,
        gitlab_user: "amr.kerkiz",
        role: "Back-end",
        bio: "Amro is 20 years old and a junior at UT studying Computer Science. He is from Austin, Texas.",
        unit_tests: 12,
        gitlab_name: "amrkerkiz",
        email: "amr.kerkiz@gmail.com"
    },
    {
        name: "Jae Garcia-Herrera",
        image: Jae,
        gitlab_user: "jaegarciah",
        role: "Back-end",
        bio: "Jae is a senior majoring in CS at UT Austin. He enjoys drumming, cooking, and live music!",
        unit_tests: 2,
        gitlab_name: "Jae Garcia",
        email: "jaegarcia019@gmail.com"
    },
    {
        name: "Jasmine Wang",
        image: Jasmine,
        gitlab_user: "jasmineywang",
        role: "Full-stack",
        bio: "Jasmine is a junior CS major from Austin, TX. She enjoys boba, grocery shopping and going to concerts!",
        unit_tests: 1,
        gitlab_name: "jasmineywang",
        email: "jasminewang@utexas.edu"
    },
    {
        name: "Aneesh Chakka",
        image: Aneesh,
        gitlab_user: "aneeshchakka",
        role: "Back-end",
        bio: "Aneesh is a junior CS major at UT Austin. He is 20 years old and is from Round Rock, TX.",
        unit_tests: 2,
        gitlab_name: "aneeshchakka",
        email: "aneesh.chakka@gmail.com"
    },
    {
        name: "Dewayne Benson",
        image: Dewayne,
        gitlab_user: "dtbenson",
        role: "Front-end",
        bio: "Dewayne is a junior computer science major at UT Austin. He loves cookies, cats, and bothering people!",
        unit_tests: 24,
        gitlab_name: "Dewayne Benson",
        email: "dewayne.benson425@gmail.com"
    }
]
