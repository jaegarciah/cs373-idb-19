// code from ConcertsFor.me: https://gitlab.com/jacksonnakos/cs373-idb/-/blob/7cc34c00f4149980fcb34364119151cba6fad1ea/front-end/src/utils/GitLabUtils.tsx
export interface Counts {
    all: number;
    closed: number;
    opened: number;
}

export interface Statistics {
    counts: Counts;
}

export interface StatisticsCall {
    statistics: Statistics;
}

export interface ContributorInfo {
    name: string;
    email: string;
    commits: number;
    additions: number;
    deletions: number;
}

export function findContributorInfoWithName(contributorInfoList: ContributorInfo[], gitlab_name: String, gitlab_email: String) {
    if (contributorInfoList === undefined || contributorInfoList.length === 0) {
        return null
    }
    for (var i = 0; i < contributorInfoList.length; i++) {
        let contributorInfo: ContributorInfo = contributorInfoList[i]
        if (contributorInfo.name === gitlab_name || contributorInfo.email === gitlab_email) {
            return contributorInfo
        }
    }
    console.log(contributorInfoList)
    throw new Error("Name " + gitlab_name + " not found in contributorInfoList!!!");
}

export function getTotalCommits(contributorInfoList: ContributorInfo[]) {
    if (contributorInfoList === undefined || contributorInfoList.length === 0) {
        return 0
    }
    let result = 0
    for (var i = 0; i < contributorInfoList.length; i++) {
        let contributorInfo: ContributorInfo = contributorInfoList[i]
        result += contributorInfo.commits
    }
    return result
}
