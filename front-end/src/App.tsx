
import { Route, Routes } from "react-router-dom";

import NavBar from './components/NavBar';
import Home from './pages/Home';
import About from './pages/About';
import Artists from './pages/Artists';
import Concerts from './pages/Concerts';
import Cities from './pages/Cities';
import Search from './pages/Search';
import { ArtistDetail } from "./pages/ArtistDetail";
import { CityDetail } from "./pages/CityDetail";
import { ConcertDetail } from "./pages/ConcertDetail";
import { createTheme, ThemeProvider } from "@mui/material";
import Visualizations from "./pages/Visualizations";

function App() {
  document.body.style.backgroundColor = "#AABCEE"
  return (
      <div>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/about" element={<About/>} />
          <Route path="/artists" element={<Artists/>}/>
          <Route path="/concerts" element={<Concerts/>}/>
          <Route path="/cities" element={<Cities/>}/>
          <Route path="/artistdetail/:id" element={<ArtistDetail />} />
          <Route path="/citydetail/:id" element={<CityDetail/>}/>
          <Route path="/concertdetail/:id" element={<ConcertDetail/>}/>
          {/* site-wide search */}
          <Route path="/search/:query" element={<Search />} /> 
          <Route path="/visualizations" element={<Visualizations/>}/>
        </Routes>
      </div>  
  )
}

export default App;
