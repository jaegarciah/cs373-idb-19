import { Container, Row, Col, Carousel } from 'react-bootstrap'
import { CategoryCard } from '../components/CategoryCard'
import '../styles/homepage.css'
import concertPhoto from '../assets/images/concertPhoto.jpeg'
import cityPhoto from '../assets/images/cityPhoto.jpeg'
import harryStyles from '../assets/images/harryStyles.jpeg'
import concert1 from '../assets/images/concert1.jpg'
import concert2 from '../assets/images/concert2.jpg'
import concert3 from '../assets/images/concert3.jpg'

function Home() {
    return (
        <div id = "stylish">
            <Carousel>
                <Carousel.Item>
                    <img className="d-block w-100 img-carousel" src={concert1} />
                </Carousel.Item>
                <Carousel.Item>
                    <img className="d-block w-100 img-carousel" src={concert2} />
                </Carousel.Item>
                <Carousel.Item>
                    <img className="d-block w-100 img-carousel" src={concert3} />
                </Carousel.Item>
            </Carousel>
            
            <Container className="my-4">
                <Row className='mb-3 text-center'>
                <h2>Join the music community and never miss a beat!</h2>
                </Row>
                
                <Row>
                    <Col>
                    <CategoryCard 
                        title="Artists"
                        subtitle=""
                        text="Discover your favorite artists!"
                        imgURL={harryStyles}
                        linkURL="/artists"
                    />
                    </Col>
                    
                    <Col>
                    <CategoryCard 
                        title="Concerts"
                        subtitle=""
                        text="Find the hottest events near you!"
                        imgURL={concertPhoto}
                        linkURL="/concerts"
                    />
                    </Col>
                    <Col>
                        <CategoryCard 
                            title="Cities"
                            subtitle=""
                            text="Venture to new places!"
                            imgURL={cityPhoto}
                            linkURL="/cities"
                        />
                    </Col>
                </Row>
                
            </Container>
        </div>
    )
}

export default Home