import { Col, Container, Row, Spinner, Tab, Tabs } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import axios from "axios";
import { useEffect, useState } from "react";
import ArtistCard from "../components/ArtistCard";
import CityCard from "../components/CityCard";
import ConcertCard from "../components/ConcertCard";

const Search = () => {
  const [artistData, setArtistData] = useState<any[]>([]);
  const [concertData, setConcertData] = useState<any[]>([]);
  const [cityData, setCityData] = useState<any[]>([]);

  const [artistLoaded, setArtistLoaded] = useState(false);
  const [concertLoaded, setConcertLoaded] = useState(false);
  const [cityLoaded, setCityLoaded] = useState(false);
  const location = useLocation();
  const query = location.pathname.split("/search/").at(-1) ?? "";
  const userQuery = query.replace(/\//g, "")
  const queryRE = new RegExp(`(?:${userQuery.replaceAll("%20", "|")})`, "i");
  const client = axios.create({
    baseURL: "https://api.musicmap.me/",
  });

  // load data from query & get results for all 3 models
  const getArtistResults = async () => {
    if (!artistLoaded) {
        console.log(userQuery)
        await client
            .get(`artists?search=${userQuery}`)
            .then((response) => {
                setArtistData(response.data['artists']);
            })
            .catch((err) => console.log(err));
            setArtistLoaded(true)
    }
  };

  const getConcertResults = async () => {
    if (!concertLoaded) {
        await client
            .get(`concerts?search=${userQuery}`)
            .then((response) => {
                setConcertData(response.data['concerts']);
            })
            .catch((err) => console.log(err));
            setConcertLoaded(true)
    }
  };

  const getCityResults = async () => {
    if (!cityLoaded) {
        await client
            .get(`cities?search=${userQuery}`)
            .then((response) => {
                setCityData(response.data['cities']);
            })
            .catch((err) => console.log(err));
            setCityLoaded(true)
    }
  };

  // this will run exactly once
  useEffect(() => {
    getArtistResults();
    getConcertResults();
    getCityResults();
  }, [artistData, artistLoaded, concertData, concertLoaded, cityData, cityLoaded]);

  return (
    <Container className="justify-content-center my-4">
      <center><h1>Search Results</h1></center>
      <Tabs defaultActiveKey="Artists" fill>
        <Tab eventKey="Artists" title="Artists">
          <Row xl={4} lg={3} md={2} sm={1} xs={1}>
            {artistLoaded ? (
              artistData.map((artist: any) => (
                <Col className = "d-flex align-self-stretch" key={artist.id}>
                <ArtistCard key = {artist.id}
                    id = {artist.id}
                    name = {artist.name}
                    birthday = {artist.birthday}
                    genre = {artist.genre}
                    followers = {artist.followers}
                    popularity = {artist.popularity}
                    image_url = {artist.img_url}
                    regex = {queryRE}/>
                </Col>
              ))
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Tab>
        
        <Tab eventKey="Concerts" title="Concerts">
          <Row xl={4} lg={3} md={2} sm={1} xs={1}>
            {
            concertLoaded ? (
              concertData.map((concert: any) => (
                <Col  className = "d-flex align-self-stretch" key={concert.id}>
                    <ConcertCard key = {concert.id}
                        id = {concert.id}
                        name = {concert.name}
                        state = {concert.state}
                        city = {concert.city}
                        venue = {concert.venue}
                        date = {concert.date}
                        time = {concert.time}
                        timezone = {concert.timezone}
                        img_url = {concert.img_url}
                        regex = {queryRE}
                    />
                    </Col>
              ))
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Tab>
        <Tab eventKey="Cities" title="Cities">
          <Row xl={4} lg={3} md={2} sm={1} xs={1}>
            {cityLoaded ? (
              cityData.map((city: any) => (
                <Col className = "d-flex align-self-stretch" key={city.id}>
                    <CityCard key = {city.id}
                    id = {city.id}
                    name = {city.long_name.slice(0, -4)}
                    population = {city.population}
                    budget = {city.budget}
                    safety = {city.safety}
                    rating = {city.rating}
                    image_url = {city.photo}
                    regex = {queryRE}/>
                </Col>
              ))
            ) : (
              <Spinner animation="grow" />
            )}
          </Row>
        </Tab>
      </Tabs>
    </Container>
  );
};

export default Search;
