import { Container, Card, Row, Col } from 'react-bootstrap'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'
import axios from 'axios'

export function ConcertDetail() {

    const client = axios.create({
        baseURL: "https://api.musicmap.me/",
    });

    const { id } = useParams()
    const [concert, setConcert] = useState<any>([])
    const [cities, setCities] = useState<any[]>([])
    const [artists, setArtists] = useState<any[]>([])
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const fetchConcert = async () => {
            if (concert === undefined || concert.length === 0) {
                await client
                    .get(`concerts/${id}`)
                    .then((response) => {
                        setConcert(response.data["concert"][0])
                        setCities(response.data["city"])
                        setArtists(response.data["artists"])
                    })
                    .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchConcert();
        }, [concert, cities, artists, loaded]);

    let name = concert.name
    let img_url = concert.img_url
    let city = concert.city
    let state = concert.state
    let artist_list = concert.artists
    let latitude = concert.latitude
    let longitude = concert.longitude
    let venue = concert.venue
    let venue_addr = concert.venue_addr
    let venue_img_url = concert.venue_img_url
    let date = concert.date
    let time = concert.time
    let timezone = concert.timezone
    let tm_url = concert.tm_url
    let hasRelatedCity = cities.length > 0
    let hasRelatedArtist = artists.length > 0

    return (
        <div id = "stylish">
        <Container style ={{marginTop:50, display: 'flex', justifyContent: 'center'}}>
            <Row>
                <Col>
                <Card style={{ width: '50rem' }}>
                    <Card.Img variant="top" src={img_url} />
                    <Card.Body>
                        <h1>{name}</h1>
                        <Card.Text>
                            Date: {date} <br></br>
                            Time: {time} <br></br>
                            Timezone: {timezone} <br></br>
                            Location: {city + ", " + state} <br></br>
                            <a href={tm_url} target="_blank" rel="noreferrer">
                    Ticketmaster Info</a>
                        </Card.Text>
                        <h3>Venue</h3>
                        <p>{venue}<br></br>
                            Address: {venue_addr}</p>
                        <iframe
                        width="100%"
                        height="300"
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCT2JTfh-QdNipJhEA1q9LvEZ0WV2rvaqY&q=" + concert.venue}
                        allowFullScreen>
                        </iframe>
                        <h3>Connections</h3>
                        <div>
                            { hasRelatedArtist ? (<Link to={`/artistdetail/${artists[0].id}`}>Related Artist: {artists[0].name}</Link>) : (<p>Found no related artists</p>)}
                        </div>
                        <div>
                            { hasRelatedCity ? (<Link to={`/citydetail/${cities[0].id}`}>Related City: {cities[0].name}</Link>) : (<p>Found no related cities</p>)}
                        </div>
                    </Card.Body>
                </Card>
                </Col>
                <div style = {{display: 'flex', justifyContent: 'center', marginTop: '10px'}}>
                
                </div>

            </Row>
            
        </Container>
        </div>
    )
}