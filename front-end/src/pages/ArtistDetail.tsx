import { Container, Card, Row, Col } from 'react-bootstrap'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'
import SpotifyPlayer from 'react-spotify-player'
import axios from 'axios'
import { Timeline } from 'react-twitter-widgets'

export function ArtistDetail() {

    const client = axios.create({
        baseURL: "https://api.musicmap.me/",
    });

    const { id } = useParams()
    const [artist, setArtist] = useState<any>([])
    const [cities, setCities] = useState<any[]>([])
    const [concerts, setConcerts] = useState<any[]>([])
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const fetchArtist = async () => {
            if (artist === undefined || artist.length === 0) {
                await client
                    .get(`artists/${id}`)
                    .then((response) => {
                        setArtist(response.data["artist"][0])
                        setCities(response.data["cities"])
                        setConcerts(response.data["concerts"])
                    })
                    .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchArtist();
        }, [artist, cities, concerts, loaded]);

    function ensureName(name: string) {
        if (name == null) {
            return ""
        } else {
            return name
        }
    }

    function noSpaces(name:string) {
        if (typeof(name) == undefined || name == null) {
            return " "
        } else {
            let build = ""
            for (let i = 0; i < name.length; i++) {
                const character = name.charAt(i)
                build += character
            
            }
            return build
        }
    }

    let name = artist.name
    let image_url = artist.img_url
    let genre = artist.genre
    let followers = artist.followers
    let birthday = artist.birthday
    let popularity = artist.popularity
    let spotify_id = artist.spotify_id
    let spotify_uri = `spotify:artist:${spotify_id}`
    let player_size = {width: '100%', height: '650'}
    let hasRelatedConcert = concerts.length > 0
    let hasRelatedCity = cities.length > 0
    let twitter = noSpaces(artist.twitter_user)
    console.log(twitter)

    return (
        <div id = "stylish">
        <Container style ={{marginTop:20, justifyContent: 'center'}}>
        <Row>
                <Col className='py-3' xl={{ order: 'first'}}>
                    <Timeline
                        dataSource={{ sourceType: "profile", screenName: twitter }}
                        renderError={_err =>""}
                        options={{ height: "650" }}
                    /></Col>
            
                <Col className='py-3' xl={5} lg={{ order: 'first'}} sm={{ order: 'first' }} md={{ order: 'first' }} xs={{ order: 'first' }}>
                <Card >
                    <Card.Img variant="top" src={image_url} />
                    <Card.Body>
                        <h1>{name}</h1>
                        <Card.Text>
                            Genre: {genre} <br></br>
                            Followers: {followers}  <br></br>
                            Popularity: {popularity} <br></br>
                            Birthday: {birthday} <br></br>
                        </Card.Text>
                        
                        <h3>Connections</h3>
                        <div>
                            { hasRelatedConcert ? (<Link to={`/concertdetail/${concerts[0].id}`}>Related Concert: {concerts[0].name}</Link>) : (<p>Has no related concerts</p>)}
                        </div>
                        <div>
                            { hasRelatedCity ? (<Link to={`/citydetail/${cities[0].id}`}>Related City: {cities[0].name}</Link>) : (<p>Has no related cities</p>) }
                        
                        </div>
                    </Card.Body>
                </Card>
                </Col>
                <Col className='py-3' > 
                    <SpotifyPlayer
                        uri={spotify_uri}
                        size={player_size}
                    />
                    
                </Col>
            </Row>
        
        </Container>
        </div>
    )
}