import Container from "react-bootstrap/Container";
import Typography from "@mui/material/Typography";
import ArtistGenresChart from "../components/visualizations/ArtistGenreChart";
import CityBudgetSafetyPlot from "../components/visualizations/CityBudgetSafetyPlot";
import ConcertStateChart from "../components/visualizations/ConcertStateChart";
import TeamsStatesChart from "../components/visualizations/TeamsStatesChart";
import PlayersGamesPoints from "../components/visualizations/PlayersGamesPoints";
import ChampionshipsTeams from "../components/visualizations/ChampionshipsTeamsChart";

const Visualizations = () => {
    return (
        <Container>
            
            <h1 className="pt-5 text-center">Visualizations</h1>

            <h3 className="pt-5 text-center">Number of Artists for each Genre</h3>
            <ArtistGenresChart/>

            <h3 className="pt-5 text-center">Cities Budget and Safety Ratings</h3>
            <CityBudgetSafetyPlot/>

            <h3 className="pt-5 text-center">Number of Concerts in each State</h3>
            <ConcertStateChart/>

            <h1 className="pt-5 text-center">Provider Visualizations</h1>

            <h3 className="pt-5 text-center">Number of Teams for each State</h3>
            <TeamsStatesChart/>

            <h3 className="pt-5 text-center">Players Games and Points</h3>
            <PlayersGamesPoints/>

            <h3 className="pt-5 text-center">Championships per Team</h3>
            <ChampionshipsTeams/>
            
        </Container>
    )
    
}

export default Visualizations;

