import { Container, Card, Row, Col } from 'react-bootstrap'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { ProgressBar } from 'react-bootstrap';
import { CityStats } from '../components/CityStats'

export function CityDetail() {

    const client = axios.create({
        baseURL: "https://api.musicmap.me/",
    });

    const { id } = useParams()
    const [city, setCity] = useState<any>([])
    const [artists, setArtists] = useState<any>([])
    const [concerts, setConcerts] = useState<any[]>([])
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const fetchCity = async () => {
            if (city === undefined || city.length === 0) {
                await client
                    .get(`cities/${id}`)
                    .then((response) => {
                        setCity(response.data["city"][0])
                        setArtists(response.data["artists"])
                        setConcerts(response.data["concerts"])
                    })
                    .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchCity();
        }, [city, artists, concerts, loaded]);


    let name = city.short_name
    let long_name = city.long_name
    let img_url = city.photo
    let hasRelatedConcert = concerts.length > 0
    let hasRelatedArtist = artists.length > 0
    let population = city.population

    return (
        <div id = "stylish">
        <Container style ={{marginTop:50, display: 'flex', justifyContent: 'center'}}>
            <Row>
                <Col>
                <Card style={{ width: '50rem' }}>
                    <Card.Img variant="top" src={img_url} />
                    <Card.Body>
                        <h1>{long_name}</h1>
                        <Card.Text>
                        
                        </Card.Text>
                        <iframe
                        width="100%"
                        height="300"
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCT2JTfh-QdNipJhEA1q9LvEZ0WV2rvaqY&q=" + name}
                        allowFullScreen></iframe>
                        
                        <h3>Connections</h3>
                        <div>
                            { hasRelatedConcert ? (<Link to={`/concertdetail/${concerts[0].id}`}>Related Concert: {concerts[0].name}</Link>) : (<p>Found no related concerts</p>)}
                        </div>
                        <div>
                            { hasRelatedArtist ? (<Link to={`/artistdetail/${artists[0].id}`}>Related Artist: {artists[0].name}</Link>) : (<p>Found no related artists</p>)}
                        </div>
                    </Card.Body>
                </Card>

                </Col>
            </Row>
            <Col><div className="m-5"><CityStats
                        population = {population}
                        rating={city.rating}
                        safety={city.safety}
                        budget={city.budget}
                    /></div></Col>
            
        </Container>
        </div>
    )
}