import { Container, 
    Form, 
    FloatingLabel, 
    InputGroup, 
    Button, 
    Row,
    Col,
    Pagination, 
    Spinner} from 'react-bootstrap'
import CityCard from '../components/CityCard'
import { useState, useEffect, useRef } from 'react'
import axios from 'axios'

import FilterDropdown from '../components/FilterDropdown';
import RangeSlider from '../components/RangeSlider';

const client = axios.create({
    baseURL: "https://api.musicmap.me/",
});

const PER_PAGE = 20
const NUM_CITIES = 79
var queryRE: RegExp | null = null

function Cities() {

    const [cities, setCities] = useState<any[]>([])
    const [loaded, setLoaded] = useState(false)
    const [activePage, setActivePage] = useState(1)

    const searchQuery = useRef<HTMLInputElement>(null)

    const [sort, setSort] = useState("Sort")
    const [ascending, setAscending] = useState(false)
    const [population, setPopulation] = useState([0, 1000000])
    const [rating, setRating] = useState([0, 5])
    const [budget, setBudget] = useState([0, 10])
    const [safety, setSafety] = useState([0, 5])

    const handleSortFilter = (value:string) => {
        setSort(value.toLowerCase().replace(" ", "_"))
    }

    const handleOrderFilter = (value:string) => {
        setAscending(value == "Ascending")
    }

    const handlePopulationFilter = (value:number[]) => {
        setPopulation(value)
    }
    const handleRatingFilter = (value:number[]) => {
        setRating(value)
    }
    const handleBudgetFilter = (value:number[]) => {
        setBudget(value)
    }
    const handleSafetyFilter = (value:number[]) => {
        setSafety(value)
    }
    
    function handleClick(num: number) {
        console.log("clicked page", num)
        setActivePage(num)
        setLoaded(false)
    }

    function arraysEqual(a: number[], b: number[]): boolean {
        if (a.length !== b.length) {
          return false;
        }
        return a.every((value, index) => value === b[index]);
    }

    useEffect(() => {
        const fetchCities = async() => {
            if (!loaded) {
                var query = `cities?page=${activePage}&perPage=${PER_PAGE}`
                if (searchQuery.current != null && searchQuery.current.value != "") {
                    query += `&search=${searchQuery.current.value}`
                    queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`, "i")
                } else {
                    queryRE = null
                    if (!arraysEqual(population, [0, 1000000])) {
                        query += `&population=${population[0]}-${population[1]}`
                    }
                    if (!arraysEqual(rating, [0, 5])) {
                        query += `&rating=${rating[0]}-${rating[1]}`
                    }
                    if (!arraysEqual(budget, [0, 10])) {
                        query += `&budget=${budget[0]}-${budget[1]}`
                    }
                    if (!arraysEqual(safety, [0, 5])) {
                        query += `&safety=${safety[0]}-${safety[1]}`
                    }
                    if (sort != "Sort") {
                        query += `&sort=${sort}`
                        query += ascending ? "_asc" : "_desc"
                    }
                }
                
                console.log(query)
                await client
                  .get(query)
                  .then((response) => {
                    setCities(response.data['cities'])
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchCities();
    }, [cities, loaded]);

    let numPages = Math.ceil(NUM_CITIES / PER_PAGE)
    let items = []
    for (let num = activePage - 2; num <= activePage + 2; num++) {
        if (num > 0 && num <= numPages) {
            items.push(
                <Pagination.Item
                    key={num}
                    onClick={() => handleClick(num)}
                    active={num === activePage}>
                        {num}
                </Pagination.Item>
            )
        }
    }

    return (
        <div id = "stylish">
            <Container className="pb-4">
                {/* Search */}
                <Container className="my-5">
                    <InputGroup className="mb-3">
                        <FloatingLabel
                            controlId="searchInput"
                            label="Search Cities">
                            <Form.Control ref={searchQuery} type="search"/>
                        </FloatingLabel>
                        <Button onClick={() => setLoaded(false)}>
                        Search
                        </Button>
                    </InputGroup>
                </Container>

                {/* Filters */}
                <Form className="d-flex gap-4 justify-content-center pb-5">
                    <Form.Label>Population:</Form.Label>
                    <RangeSlider min={0} max={10000000} onChange={handlePopulationFilter} />
                    <Form.Label>Rating:</Form.Label>
                    <RangeSlider min={0} max={5} onChange={handleRatingFilter} />
                    <Form.Label>Budget:</Form.Label>
                    <RangeSlider min={0} max={10} discrete onChange={handleBudgetFilter} />
                    <Form.Label>Safety:</Form.Label>
                    <RangeSlider min={0} max={5} discrete onChange={handleSafetyFilter} />
                </Form>

                {/* Sort */}
                <Form className="d-flex gap-4 justify-content-center pb-5">
                    <FilterDropdown
                        title="Sort"
                        items={[
                            "Sort",
                            "Name",
                            "Population",
                            "Rating",
                            "Budget",
                            "Safety"
                        ]}
                        onChange={handleSortFilter}
                    />
                    <FilterDropdown
                        title="Order"
                        items={["Ascending", "Descending"]}
                        onChange={handleOrderFilter}
                    />
                    <Button onClick={() => setLoaded(false)}>
                        Submit
                    </Button>
                </Form>

                {/* Pagination */}
                <Pagination className="justify-content-center">
                    {activePage > 3 && (
                    <Pagination.First
                        key={1}
                        onClick={() => handleClick(1)}
                        active={1 === activePage}>
                        1
                    </Pagination.First>
                    )}
                    {activePage > 4 && <Pagination.Ellipsis/>}
                    {items}
                    {activePage < numPages - 3 && <Pagination.Ellipsis />}
                    {activePage < numPages - 2 && (
                    <Pagination.Last
                        key={numPages}
                        onClick={() => handleClick(numPages)}
                        active={numPages === activePage}>
                        {numPages}
                    </Pagination.Last>
                    )}
                </Pagination>
                
                {/* Card Grid */}
                <p>Showing {cities.length} out of {NUM_CITIES} instances</p>
                <Container>
                    <Row
                    xl={5}
                    lg={4}
                    md={3}
                    sm={2}
                    xs={1}
                    className="d-flex g-0 p-0 justify-content-center">
                        { loaded ? (
                            cities.map((data) => {
                                return (
                                    <Col className = "d-flex align-self-stretch" key={data.id}>
                                        <CityCard key = {data.id}
                                        id = {data.id}
                                        name = {data.long_name.slice(0, -4)}
                                        population = {data.population}
                                        budget = {data.budget}
                                        safety = {data.safety}
                                        rating = {data.rating}
                                        image_url = {data.photo}
                                        regex = {queryRE}/>
                                    </Col>
                                )
                            })) : (<Spinner animation="grow"/>) }
                    </Row>
                </Container>

                {/* Pagination */}
                <Pagination className="justify-content-center py-3">
                    {activePage > 3 && (
                    <Pagination.First
                        key={1}
                        onClick={() => handleClick(1)}
                        active={1 === activePage}>
                        1
                    </Pagination.First>
                    )}
                    {activePage > 4 && <Pagination.Ellipsis/>}
                    {items}
                    {activePage < numPages - 3 && <Pagination.Ellipsis />}
                    {activePage < numPages - 2 && (
                    <Pagination.Last
                        key={numPages}
                        onClick={() => handleClick(numPages)}
                        active={numPages === activePage}>
                        {numPages}
                    </Pagination.Last>
                    )}
                </Pagination>
            </Container>
        </div>
    )
}

export default Cities