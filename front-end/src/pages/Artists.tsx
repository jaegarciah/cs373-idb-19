import { Container, 
    Form, 
    FloatingLabel, 
    ButtonToolbar, 
    Dropdown, 
    DropdownButton,
    Row,
    Col,
    Pagination, 
    InputGroup,
    Button} from 'react-bootstrap'
import Spinner from "react-bootstrap/Spinner"
import ArtistCard from '../components/ArtistCard'
import { useState, useEffect, useRef } from 'react'
import axios from 'axios'
import FilterDropdown from '../components/FilterDropdown'

const client = axios.create({
    baseURL: "https://api.musicmap.me/",
});

const PER_PAGE = 20
const NUM_ARTISTS = 282
var queryRE: RegExp | null = null

function Artists() {

    const [artists, setArtists] = useState<any[]>([])
    const [loaded, setLoaded] = useState(false)
    const [activePage, setActivePage] = useState(1)

    const searchQuery = useRef<HTMLInputElement>(null)

    const [sort, setSort] = useState("Default");
    const [ascending, setAscending] = useState(false);
    const [gender, setGender] = useState("Gender");
    const [genre, setGenre] = useState("Genre");
    const [country, setCountry] = useState("Country");

    const handleSortFilter = (value:string) => {
        setSort(value.toLowerCase().replace(" ", "_"))
    }

    const handleOrderFilter = (value:string) => {
        setAscending(value == "Ascending")
    }

    const handleGenderFilter = (value:string) => {
        setGender(value)
    }

    const handleGenreFilter = (value:string) => {
        setGenre(value)
    }

    const handleCountryFilter = (value:string) => {
        setCountry(value)
    }

    function handleClick(num: number) {
        console.log("clicked page", num)
        setActivePage(num)
        setLoaded(false)
    }

    useEffect(() => {
        const fetchArtists = async() => {
            if (!loaded) {
                var query = `artists?page=${activePage}&perPage=${PER_PAGE}`
                if (searchQuery.current != null && searchQuery.current.value != "") {
                    query += `&search=${searchQuery.current.value}`
                    queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`, "i")
                } else {
                    queryRE = null
                    if (gender != "Gender") {
                        query += `&gender=${gender}`
                    }
                    if (genre != "Genre") {
                        query += `&genre=${genre}`
                    }
                    if (country != "Country") {
                        query += `&country=${country}`
                    }
                    if (sort != "Sort") {
                        query += `&sort=${sort}`
                        query += ascending ? "_asc" : "_desc"
                    }
                }
                await client
                  .get(query)
                  .then((response) => {
                    setArtists(response.data['artists'])
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchArtists();
    }, [artists, loaded]);

    let numPages = Math.ceil(NUM_ARTISTS / PER_PAGE)
    let items = []
    for (let num = activePage - 2; num <= activePage + 2; num++) {
        if (num > 0 && num <= numPages) {
            items.push(
                <Pagination.Item
                    key={num}
                    onClick={() => handleClick(num)}
                    active={num === activePage}>
                        {num}
                </Pagination.Item>
            )
        }
    }

    return (
    <div id = "stylish">
        <Container className="pb-4">
            {/* Search */}
            <Container className="my-5">
                <InputGroup className="mb-3">
                    <FloatingLabel
                        controlId="searchInput"
                        label="Search Artists">
                        <Form.Control ref={searchQuery} type="search"/>
                    </FloatingLabel>
                    <Button onClick={() => setLoaded(false)}>
                    Search
                    </Button>
                </InputGroup>
            </Container>

            {/* Filters & Sort */}
            <Form className="d-flex gap-4 justify-content-center pb-5">
                <FilterDropdown
                    title="Gender"
                    items={[
                        "Gender",
                        "Female",
                        "Male",
                        "non-binary",
                        "other",
                        "NA"
                    ]}
                    onChange={handleGenderFilter}
                />
                <FilterDropdown
                    title="Genre"
                    items={[
                        "Genre",
                        "pop",
                        "rap",
                        "indie",
                        "hip hop",
                        "country",
                        "k-pop",
                        "rock",
                        "metal",
                        "emo",
                        "edm",
                        "reggae",
                        "funk",
                        "contemporary",
                        "electro",
                        "folk",
                        "dance",
                        "atl",
                        "r&b"
                    ]}
                    onChange={handleGenreFilter}
                />
                <FilterDropdown
                    title="Country"
                    items={[
                        "Country",
                        "US",
                        "AR",
                        "AU",
                        "CA",
                        "CO",
                        "ES",
                        "FR",
                        "GB",
                        "IE",
                        "JM",
                        "KR",
                        "MX",
                        "NL",
                        "NO",
                        "NZ",
                        "PR"
                    ]}
                    onChange={handleCountryFilter}
                />
                <FilterDropdown
                    title="Sort"
                    items={[
                        "Default",
                        "Name",
                        "Birthday",
                        "Popularity",
                        "Followers"
                    ]}
                    onChange={handleSortFilter}
                />
                <FilterDropdown
                    title="Order"
                    items={["Ascending", "Descending"]}
                    onChange={handleOrderFilter}
                />
                <Button onClick={() => setLoaded(false)}>
                    Submit
                </Button>
            </Form>
        
        {/* Pagination */}
        <Pagination className="justify-content-center">
            {activePage > 3 && (
            <Pagination.First
                key={1}
                onClick={() => handleClick(1)}
                active={1 === activePage}>
                1
            </Pagination.First>
            )}
            {activePage > 4 && <Pagination.Ellipsis/>}
            {items}
            {activePage < numPages - 3 && <Pagination.Ellipsis />}
            {activePage < numPages - 2 && (
            <Pagination.Last
                key={numPages}
                onClick={() => handleClick(numPages)}
                active={numPages === activePage}>
                {numPages}
            </Pagination.Last>
            )}
        </Pagination>

        {/* Card Grid */}
        <p>Showing {artists.length} out of 282 instances</p>

        <Container >
            <Row
            xl={5}
            lg={4}
            md={3}
            sm={2}
            xs={1}
            className="d-flex g-0 p-0 justify-content-center">
                { loaded ? (
                    artists.map((data: any) => {
                        return ( 
                            <Col className = "d-flex align-self-stretch" key={data.id}>
                                <ArtistCard key = {data.id}
                                    id = {data.id}
                                    name = {data.name}
                                    birthday = {data.birthday}
                                    genre = {data.genre}
                                    followers = {data.followers}
                                    popularity = {data.popularity}
                                    image_url = {data.img_url}
                                    regex = {queryRE}/>
                            </Col>
                        )
                    })) : (<Spinner animation="grow"/>)}
            </Row>
        </Container>

        {/* Pagination */}
        <Pagination className="justify-content-center py-3">
            {activePage > 3 && (
            <Pagination.First
                key={1}
                onClick={() => handleClick(1)}
                active={1 === activePage}>
                1
            </Pagination.First>
            )}
            {activePage > 4 && <Pagination.Ellipsis/>}
            {items}
            {activePage < numPages - 3 && <Pagination.Ellipsis />}
            {activePage < numPages - 2 && (
            <Pagination.Last
                key={numPages}
                onClick={() => handleClick(numPages)}
                active={numPages === activePage}>
                {numPages}
            </Pagination.Last>
            )}
        </Pagination>
            
    </Container>
    </div>
    )
}

export default Artists