import React, { useState, useEffect } from 'react';
import { Container, Row, Col, ListGroup, Badge } from 'react-bootstrap';
import { team_list } from '../data/teamInfo';
import { apiData, toolsData } from '../data/projectData';
import DeveloperCard from '../components/DeveloperCard';
import APICard from '../components/APICard';
import axios from 'axios'
import { ContributorInfo, findContributorInfoWithName, getTotalCommits, StatisticsCall } from '../GitLabUtils';

// glpat-c5CNB7efhd64_G8fT_Ao
// code from ConcertsFor.me: https://gitlab.com/jacksonnakos/cs373-idb/-/blob/7cc34c00f4149980fcb34364119151cba6fad1ea/front-end/src/utils/GitLabUtils.tsx

function About() {

    const [totalNumIssues, setTotalNumIssues] = useState(0)
    useEffect(() => {
        fetch('https://gitlab.com/api/v4/projects/43316789/issues_statistics')
            .then(function (response) {
                return response.text();
            })
            .then(function (myJson) {
                let stats: StatisticsCall = JSON.parse(myJson)
                let result = stats.statistics.counts.all
                setTotalNumIssues(result)
            });
    }, [])

    const [contributorInfoList, setContributorInfo] = useState([] as ContributorInfo[])
    useEffect(() => {
        fetch('https://gitlab.com/api/v4/projects/43316789/repository/contributors')
            .then(function (response) {
                return response.text();
            })
            .then(function (myJson) {
                let contributorInfoList: ContributorInfo[] = JSON.parse(myJson)
                setContributorInfo(contributorInfoList)
            });
    }, [])


    return (
        <div id="stylish">

            <Container className="text-center mt-5">
                <h2>What is MusicMap?</h2>
                <p className="mx-auto">
                MusicMap helps fans stay in tune with music across the country. Find new artists, upcoming events, 
                and the hottest cities around! To start exploring, check out one of our tabs!
                </p>
            </Container>
            <Container className="text-center mt-5">
                <h2>Striking a Chord with Musical Connections</h2>
                <p className="mx-auto">
                MusicMap integrates data from a variety of sources to present useful connections between artists, 
                concerts, and cities. Through these connections, you can expand your horizons by discovering new artists 
                who are performing near you or learning more about a city that you're going to an event in. 
                With MusicMap, you will never miss a beat in the music community! 
                </p>
            </Container>


            <Container className="text-center mt-5">
                <h1>Meet the Team!</h1>
                <Row
                    xs={1}
                    sm={2}
                    md={3}
                    xl={3}
                    className="g-5 p-1 justify-content-center">
                    {team_list.map((member, index) => {
                        let contributorInfo = findContributorInfoWithName(contributorInfoList, member.gitlab_name, member.email)
                        let commits = 0
                        if (contributorInfo != null) {
                            commits = (contributorInfo as ContributorInfo).commits
                        }
                    return (
                        <Col className="d-flex align-self-stretch">
                        <DeveloperCard devInfo={member} commits={commits} />
                        </Col>
                    )
                    })}
                </Row>
            </Container>


            <Container style= {{backgroundColor: 'whitesmoke'}} className="text-center mt-5 py-3">
                <h1>Overall Repository Stats</h1>
                <Row className="p-4">
                <Col className="d-flex justify-content-center">
                    <h2>Total Commits: {getTotalCommits(contributorInfoList)}</h2>
                </Col>
                <Col className="d-flex justify-content-center">
                    <h2>Total Issues: {totalNumIssues}</h2>
                </Col>
                <Col className="d-flex justify-content-center">
                    <h2>Total Tests: {41}</h2>
                </Col>
            </Row>
            <h2>
                <a href="https://documenter.getpostman.com/view/25859804/2s93CEvGRq" target="_blank" rel="noreferrer">
                    Postman Documentation
                </a>
            </h2>

                
            </Container>
            <Container className="text-center mt-5" >
                <h1>APIs</h1>
                <center>
                <Row
                    xs={1}
                    sm={2}
                    md={3}
                    xl={3}
                    className="g-4 p-4 justify-content-center">
                    {apiData.map((props) => {
                    return (
                        <Col className="d-flex align-self-stretch">
                            <APICard 
                                name = {props.name}
                                image = {props.image}
                                description = {props.description}
                                link = {props.link}
                            />
                        </Col>
                    )
                    })}
                </Row></center>
            </Container>

            
            <Container className="text-center my-5 pb-5">
                <h1>Tools</h1>
                <center>
                <Row
                    xs={1}
                    sm={2}
                    md={2}
                    lg={3}
                    xl={4}
                    className="g-4 p-1 justify-content-center">
                    {toolsData.map((props) => {
                    return (
                        <Col className="d-flex align-self-stretch">
                            <APICard 
                                name = {props.name}
                                image = {props.image}
                                description = {props.description}
                                link = {props.link}
                            />
                        </Col>
                    )
                    })}
                </Row></center>
            </Container>
            
        </div>
    )
}

export default About