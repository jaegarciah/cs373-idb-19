import { Container, 
            Form, 
            FloatingLabel, 
            ButtonToolbar, 
            Dropdown, 
            DropdownButton, Col, Row, Spinner, InputGroup, Button} from 'react-bootstrap'
import ConcertCard from '../components/ConcertCard'
import { useState, useEffect, useMemo, useRef } from 'react'
import { Pagination } from 'react-bootstrap'
import axios from 'axios';
import FilterDropdown from '../components/FilterDropdown';

const client = axios.create({
    baseURL: "https://api.musicmap.me/",
});

const PER_PAGE = 20
const NUM_CONCERTS = 2243
var queryRE: RegExp | null = null

function Concerts() {

    const [concerts, setConcerts] = useState<any[]>([])
    const [loaded, setLoaded] = useState(false)
    const [activePage, setActivePage] = useState(1)

    const searchQuery = useRef<HTMLInputElement>(null)

    const [sort, setSort] = useState("Sort")
    const [ascending, setAscending] = useState(false)
    const [timezone, setTimezone] = useState("Timezone")
    
    const handleSortFilter = (value:string) => {
        setSort(value.toLowerCase().replace(" ", "_"))
    }

    const handleOrderFilter = (value:string) => {
        setAscending(value == "Ascending")
    }

    const handleTimezoneFilter = (value:string) => {
        setTimezone(value)
    }


    function handleClick(num: number) {
        console.log("clicked page", num)
        setActivePage(num)
        setLoaded(false)
    }

    useEffect(() => {
        const fetchArtists = async() => {
            if (!loaded) {
                var query = `concerts?page=${activePage}&perPage=${PER_PAGE}`
                if (searchQuery.current != null && searchQuery.current.value != "") {
                    query += `&search=${searchQuery.current.value}`
                    queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`, "i")
                } else {
                    queryRE = null
                    if (timezone != "Timezone") {
                        query += `&timezone=${timezone}`
                    }
                    if (sort != "Sort") {
                        query += `&sort=${sort}`
                        query += ascending ? "_asc" : "_desc"
                    }
                }
                console.log(query)
                await client
                  .get(query)
                  .then((response) => {
                    setConcerts(response.data['concerts'])
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
            }
        };
        fetchArtists();
    }, [concerts, loaded]);

    let numPages = Math.ceil(NUM_CONCERTS / PER_PAGE)
    let items = []
    for (let num = activePage - 2; num <= activePage + 2; num++) {
        if (num > 0 && num <= numPages) {
            items.push(
                <Pagination.Item
                    key={num}
                    onClick={() => handleClick(num)}
                    active={num === activePage}>
                        {num}
                </Pagination.Item>
            )
        }
    }

    return (
        <div id="stylish">
            <Container className="pb-4">
                {/* Search */}
                <Container className="my-5">
                    <InputGroup className="mb-3">
                        <FloatingLabel
                            controlId="searchInput"
                            label="Search Concerts">
                            <Form.Control ref={searchQuery} type="search"/>
                        </FloatingLabel>
                        <Button onClick={() => setLoaded(false)}>
                        Search
                        </Button>
                    </InputGroup>
                </Container>         

                {/* Filter & Sort */}
                <Form className="d-flex gap-4 justify-content-center pb-5">
                    <FilterDropdown
                        title="Timezone"
                        items={[
                            "Timezone",
                            "America/Chicago",
                            "America/New_York",
                            "America/Los_Angeles",
                            "America/Denver",
                            "America/Phoenix",
                            "Pacific/Honolulu",
                            "America/Indianapolis",
                            "America/Tijuana",
                            "America/Detroit"
                        ]}
                        onChange={handleTimezoneFilter}
                    />
                    <FilterDropdown
                        title="Sort"
                        items={[
                            "Sort",
                            "Name",
                            "Date",
                            "Time",
                            "Venue",
                            "Timezone"
                        ]}
                        onChange={handleSortFilter}
                    />
                    <FilterDropdown
                        title="Order"
                        items={["Ascending", "Descending"]}
                        onChange={handleOrderFilter}
                    />
                    <Button onClick={() => setLoaded(false)}>
                        Submit
                    </Button>
                </Form>
                
                {/* Pagination */}
                <Pagination className="justify-content-center">
                        {activePage > 3 && (
                        <Pagination.First
                            key={1}
                            onClick={() => handleClick(1)}
                            active={1 === activePage}>
                            1
                        </Pagination.First>
                        )}
                        {activePage > 4 && <Pagination.Ellipsis/>}
                        {items}
                        {activePage < numPages - 3 && <Pagination.Ellipsis />}
                        {activePage < numPages - 2 && (
                        <Pagination.Last
                            key={numPages}
                            onClick={() => handleClick(numPages)}
                            active={numPages === activePage}>
                            {numPages}
                        </Pagination.Last>
                        )}
                </Pagination>

                {/* Card Grid */}
                <p>Showing {concerts.length} out of 2243 instances</p>
                <Container style={{display: 'flex'}}>

                <Row
                xl={5}
                lg={4}
                md={3}
                sm={2}
                xs={1}
                className="d-flex g-0 p-0 justify-content-center">
                    { loaded ? (
                        concerts.map((data: any) => {
                        return (
                            <Col  className = "d-flex align-self-stretch" key={data.id}>
                            <ConcertCard key = {data.id}
                                id = {data.id}
                                name = {data.name}
                                state = {data.state}
                                city = {data.city}
                                venue = {data.venue}
                                date = {data.date}
                                time = {data.time}
                                timezone = {data.timezone}
                                img_url = {data.img_url}
                                regex = {queryRE}
                            />
                            </Col>
                            )
                        })) : (<Spinner animation="grow"/>)} 
                    </Row>
                </Container>

                {/* Pagination */}
                <Pagination className="justify-content-center py-3">
                        {activePage > 3 && (
                        <Pagination.First
                            key={1}
                            onClick={() => handleClick(1)}
                            active={1 === activePage}>
                            1
                        </Pagination.First>
                        )}
                        {activePage > 4 && <Pagination.Ellipsis/>}
                        {items}
                        {activePage < numPages - 3 && <Pagination.Ellipsis />}
                        {activePage < numPages - 2 && (
                        <Pagination.Last
                            key={numPages}
                            onClick={() => handleClick(numPages)}
                            active={numPages === activePage}>
                            {numPages}
                        </Pagination.Last>
                        )}
                </Pagination>
            </Container>

        </div>
    )
}

export default Concerts