import { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import { StatisticsCall } from '../GitLabUtils';

function DeveloperCard(props:any) {

    const {
        name,
        image,
        gitlab_user,
        role,
        bio,
        unit_tests,
    } = props.devInfo

    // code from ConcertsFor.me: https://gitlab.com/jacksonnakos/cs373-idb/-/blob/main/front-end/src/components/IndividualAbout.tsx
    const [issuesCount, setIssuesCount] = useState(0)
    useEffect(() => {
        fetch('https://gitlab.com/api/v4/projects/43316789/issues_statistics?assignee_username=' + gitlab_user).then(function (response) {
            return response.text();
        })
            .then(function (myJson) {
                let stats: StatisticsCall = JSON.parse(myJson)
                let result = stats.statistics.counts.all
                setIssuesCount(result)
            });
    }, [])

  return (
    <Container style={{ width: "100%", marginTop: 10 }}>
        <Card className = "card">
          <Card.Img variant="top" src={image} />
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>@{gitlab_user}</Card.Subtitle>
            <Card.Text>{role}</Card.Text>
            <Card.Text>{bio}</Card.Text>
          </Card.Body>
          <Card.Footer>
            <Card.Text>
                Commits: {props.commits} <br></br>
                Issues: {issuesCount} <br></br>
                Unit Tests: {unit_tests} <br></br>
            </Card.Text>
          </Card.Footer>
        </Card>
    </Container>
  )
}

export default DeveloperCard