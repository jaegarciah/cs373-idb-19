import { Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import { Highlight } from 'react-highlight-regex';
import { Link } from 'react-router-dom';


function CityCard(props:any) {
  
    let id = props.id
    let name = props.name
    let state = props.state
    let population = props.population
    let budget = props.budget
    let safety = props.safety
    let rating = props.rating
    let image_url = props.image_url
    let regex = props.regex

    function highlightText(input:string) {
      if (regex != null) {
        return <Highlight match={regex} text={input} />
      }
      return input
    }

  return (
    <Container style={{ width: "100%", marginTop: 20 }}>
        <Card>
          <Card.Img variant="top" src={image_url} />
          <Card.Body>
            <Card.Title>{highlightText(name)}</Card.Title>
            <Card.Text>
              Population: {highlightText(population.toLocaleString())} <br></br>
              Budget: {highlightText(String(budget))}  <br></br>
              Safety: {highlightText(String(safety))} <br></br>
              Rating: {highlightText(String(rating))} <br></br>
          </Card.Text>
          <Link to={`/citydetail/${id}`}><Button>More Info</Button></Link>
          </Card.Body>
        </Card>
    </Container>
  )
}

export default CityCard