import { Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import { Highlight } from 'react-highlight-regex';
import { Link } from 'react-router-dom';

function ArtistCard(props:any) {
  
    let id = props.id
    let name = props.name
    let image_url = props.image_url
    let bday = props.birthday
    let genre = props.genre
    let followers = props.followers
    let popularity = props.popularity
    let regex = props.regex

    function highlightText(input:string) {
      if (regex != null) {
        return <Highlight match={regex} text={input} />
      }
      return input
    }

  return (
    <Container style={{ width: "100%", marginTop: 20 }}>
        <Card>
          <Card.Img variant="top" src={image_url} />
          <Card.Body>
            <Card.Title>{highlightText(name)}</Card.Title>
            <Card.Text>
              Genre: {highlightText(String(genre))} <br></br>
              Followers: {highlightText(followers.toLocaleString())}  <br></br>
              Popularity: {highlightText(String(popularity))} <br></br>
              Birthday: {highlightText(String(bday))} <br></br>
          </Card.Text>
          <Link to={`/artistdetail/${id}`}><Button>More Info</Button></Link>
          </Card.Body>
        </Card>
    </Container>
  )
}

export default ArtistCard