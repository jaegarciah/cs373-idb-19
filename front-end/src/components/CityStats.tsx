
import { Card, Button } from 'react-bootstrap'
import { ProgressBar } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'
import { BrowserRouter } from 'react-router-dom';
import { Rating } from 'react-simple-star-rating'

interface CardData {
    rating: number;
    safety: number;
    budget: number;
    population: number;
}

export function CityStats(props:CardData) {

    const hStyle = { color: 'white' };

    return (
        
        <Card style={{flex:1, backgroundColor:'#1F4098', borderColor: 'black'}}>
            <center>
        <br></br>
            <h3 style = {hStyle}>City Ratings</h3>
            <Rating initialValue={props.rating}
                allowFraction = {true}
    onClick={function noRefCheck(){}}
    readonly />
          <div className="d-flex gap-4"> </div>
          <div className="d-flex gap-">
            <ProgressBar 
              className="w-100 m-3"
              min={1}
              max={8}
              now={props.budget}
              variant="success"
              label ={`Cost: ${props.budget} / 10`}
            />
          </div>
          <div className="d-flex gap-4">
            <ProgressBar
              className="w-100 m-3"
              min={1}
              max={5}
              now={props.safety}
              variant="danger"
              label={`Safety: ${props.safety} / 5`}
            />
            
          </div>
          <ProgressBar
              className="w-80 m-3"
              min={0}
              max={5}
              now={props.population}
              variant = "dark"
              label={`Population: ${props.population}`}
            />
          <div>
          </div>
        </center>
        <br></br>
        </Card>
    );
}

export default CityStats