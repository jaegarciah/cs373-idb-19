import React, { useState } from "react";

import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Container from "react-bootstrap/Container";

const FilterDropdown = (props:any) => {
  const { title, items, scroll, onChange } = props;
  const [choice, setChoice] = useState(title);

  const handleClick = (value:string) => {
    setChoice(value);
    onChange(value);
  }

  return (
    <DropdownButton title={choice} variant="success">
      <Container style={scroll ? { height: "20rem", overflowY: "scroll" } : {}}>
        {items.map((item:string, index:number) => {
          return (
            <Dropdown.Item 
              key={index}
              onClick={() => handleClick(item)}>
              {item}
            </Dropdown.Item>
          )
        })}
      </Container>
    </DropdownButton>
  )
}

export default FilterDropdown
