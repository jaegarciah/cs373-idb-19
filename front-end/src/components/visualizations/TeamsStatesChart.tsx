import { PieChart, Pie, Cell, Tooltip } from 'recharts';
import { Container } from 'react-bootstrap';
import data from "../../data/provider_visualizations/teams_visualization.json"

const TeamsStatesChart = () => {

    const chartData = Object.entries(data).map(([key, value]) => ({ name: key, value }))

    const COLORS = ["#119DA4", "#456CD9", "#FE5F55", "#9B8AE5"];

    return (
    <Container className="d-flex justify-content-center text-center" fluid="md">
        <PieChart width={700} height={500}>
            <Pie
                dataKey="value"
                nameKey="name"
                isAnimationActive={true}
                data={chartData}
                cx="50%"
                cy="50%"
                outerRadius={200}
                label
            >
                {chartData.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} /> // Assign colors to each slice based on their index.
                ))}
            </Pie>
            <Tooltip />
        </PieChart>
    </Container>);

} 

export default TeamsStatesChart;
