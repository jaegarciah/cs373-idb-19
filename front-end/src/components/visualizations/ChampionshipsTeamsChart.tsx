import { RadarChart, Radar, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Tooltip } from 'recharts';
import { Container } from 'react-bootstrap';
import data from "../../data/provider_visualizations/championships_visualization.json"

const ChampionshipsTeams = () => {

    const chartData = Object.entries(data).map(([key, value]) => ({ name: key, value }))
    
    const maxValue = Math.max(...chartData.map(item => item.value));

    return (
    <Container className="d-flex justify-content-center text-center" fluid="md">
        <RadarChart cx="50%" cy="50%" outerRadius="80%" width={900} height={700} data={chartData}>
            <PolarGrid />
            <PolarAngleAxis dataKey="name" />
            <PolarRadiusAxis domain={[0, maxValue]} /> {/* Set domain to include maximum value plus additional padding */}
            <Radar
                dataKey="value"
                fill="#9B8AE5"
                fillOpacity={0.6}
            />
            <Tooltip />
        </RadarChart>
    </Container>
    );
}

export default ChampionshipsTeams;
