import { Container } from "react-bootstrap";
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";
import data from "../../data/visualizations/cities_visualization.json";

const CityBudgetSafetyPlot = () => {

    interface frequency {
        [key: string]: number;
    }

    // find frequencies
    const frequencies:frequency = {};
    Object.entries(data).forEach(([city, values]) => {
        const { budget, safety } = values;
        const pair = `${budget}-${safety}`;
        if (pair in frequencies) {
            frequencies[pair]++;
        } else {
            frequencies[pair] = 1;
        }
    });
    const points = Object.entries(frequencies).map(([pair, frequency]) => {
        const [budget, safety] = pair.split('-').map(Number);
        return { budget, safety, frequency };
    });

  return (
    <Container className="d-flex justify-content-center">
        <ScatterChart width={600} height={500}>
        <CartesianGrid />
        <XAxis type="number" dataKey="budget" label={{ value: "Budget", position: "insideBottomRight" }} domain={[0, 10]} scale="linear" />
        <YAxis type="number" dataKey="safety" label={{ value: "Safety", angle: -90, position: "insideLeft" }} />
        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        <Scatter data={points} fill="#FE5F55"/>
        {points.map((point) => (
            <Scatter
            data={[point]}
            fill="#FE5F55"
            shape={(props) => (
            <circle
            cx={props.cx}
            cy={props.cy}
            r={Math.sqrt(point.frequency) * 5} // Use square root of frequency as radius
            stroke="#FE5F55"
            strokeWidth={2}
            fillOpacity={0.7}
            fill="#FE5F55"/>
            )}
            />
        ))}
        </ScatterChart>

    </Container>
  );
};

export default CityBudgetSafetyPlot;
