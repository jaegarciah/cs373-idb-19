import { Col, Container, Row } from 'react-bootstrap';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import data from '../../data/visualizations/artists_visualization.json';

const ArtistGenresChart = () => {

  const chartData = Object.entries(data).map(([name, value]) => ({ name, value }));

  return (
    <Container className="d-flex justify-content-center" fluid="md">
        <Row style={{ width: "100%"}}>
        
        <Col>
        <BarChart width={1200} height={400} data={chartData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" angle={-45} tick={{ fontSize: 9 }} textAnchor="end" interval={0} height={80}/>
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="value" fill="#456CD9" />
        </BarChart>
        </Col>
        </Row>
    </Container>
    
  );
};

export default ArtistGenresChart;
