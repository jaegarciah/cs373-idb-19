import { Container } from 'react-bootstrap';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import data from '../../data/visualizations/concerts_visualization.json';

const ConcertStateChart = () => {

  const chartData = Object.entries(data).map(([name, value]) => ({ name, value }));

  return (
    <Container className="d-flex justify-content-center">
        <BarChart width={1200} height={400} data={chartData}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" angle={-45} tick={{ fontSize: 9 }} textAnchor="end" interval={0} height={80}/>
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="value" fill="#119DA4" />
        </BarChart>
    </Container>
    
  );
};

export default ConcertStateChart;
