import { Tooltip, CartesianGrid, Legend, Scatter, ScatterChart, XAxis, YAxis } from 'recharts';
import data from "../../data/provider_visualizations/players_visualization.json"

const PlayersGamesPoints = () => {

    const chartData = Object.entries(data).map(([name, stats]) => ({ name, ...stats }));

    return (
        <ScatterChart width={1200} height={700} margin={{ top: 20, right: 20, bottom: 20, left: 20 }}>
        <CartesianGrid />
        <XAxis type="number" dataKey="games" />
        <YAxis type="number" dataKey="points"  />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} label="Player"/>
        <Legend />
        <Scatter name="Players" data={chartData} fill="#456CD9" /> 
      </ScatterChart>);

} 

export default PlayersGamesPoints;
