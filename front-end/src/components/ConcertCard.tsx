import React from 'react'
import { Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import { Highlight } from 'react-highlight-regex';
import { Link } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';

function ConcertCard(props:any) {
  
    let id = props.id
    let name = props.name
    let state = props.state
    let city = props.city
    let img_url = props.img_url
    let venue = props.venue
    let date = props.date
    let time = props.time
    let timezone = props.timezone
    let regex = props.regex

    function highlightText(input:string) {
      if (regex != null) {
        return <Highlight match={regex} text={input} />
      }
      return input
    }

  return (
    <Container style={{ width: "100%", marginTop: 20 }}>
        <Card>
          <Card.Img variant="top" src={img_url} />
          <Card.Body>
            <Card.Title>{highlightText(name)}</Card.Title>
            <Card.Text>
                Venue: {highlightText(venue)} <br></br>
                City: {highlightText(city + ", " + state)} <br></br>
                Date: {highlightText(date)} <br></br>
                Time: {highlightText(time)} <br></br>
                Timezone: {highlightText(timezone)} <br></br>
            </Card.Text>
          <Link to={`/concertdetail/${id}`}><Button>More Info</Button></Link>
          </Card.Body>
        </Card>
    </Container>
  )
}

export default ConcertCard