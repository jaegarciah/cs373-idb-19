import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';

function APICard(props:any) {
  
    let name = props.name
    let image = props.image
    let description = props.description
    let link = props.link

  return (
    <Container style={{ width: "100%", marginTop: 20 }}>
        <Card style={{ width: '15rem' }}>
          <Card.Img variant="top" src={image} />
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Link to={link}>More Details</Link>
          </Card.Body>
        </Card>
    </Container>
  )
}

export default APICard