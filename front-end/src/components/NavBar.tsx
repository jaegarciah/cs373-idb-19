import React, { FormEvent } from 'react';
import { Button, Container, Form, Nav, Navbar } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import musicMapLogo from '../assets/musicmaplogo.svg'

const NavBar: React.FC<{}> = () => {

    const handleSubmit = (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const form = event.currentTarget
        console.log(`search query: ${form.query.value}`)
        window.location.assign(`/search/${form.query.value}`)
    }

    return (
        <div>
            <Navbar variant="dark" expand="lg" style={{backgroundColor: "#1f4098"}}>
                <Container>
                    <Navbar.Brand href="/">
                        <img src={musicMapLogo} 
                            height="30"
                            className="d-inline-block align-top"
                            alt="logo"/>{'  '}
                        MusicMap
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            
                            <LinkContainer to="/">
                                <Nav.Link>Home</Nav.Link>
                            </LinkContainer>

                            
                            <LinkContainer to="/about/">
                                <Nav.Link>About</Nav.Link>
                            </LinkContainer>

                            
                            <LinkContainer to="/artists/">
                                <Nav.Link>Artists</Nav.Link>
                            </LinkContainer>

                            
                            <LinkContainer to="/concerts/">
                                <Nav.Link>Concerts</Nav.Link>
                            </LinkContainer>

                            
                            <LinkContainer to="/cities/">
                                <Nav.Link>Cities</Nav.Link>
                            </LinkContainer>

                            <LinkContainer to="/visualizations/">
                                <Nav.Link>Visualizations</Nav.Link>
                            </LinkContainer>
                        </Nav>
                        <Container className="d-flex justify-content-end">
                            <Form onSubmit={handleSubmit} className="d-flex">
                            <Form.Control
                                style={{ width: "20vw" }}
                                type="search"
                                name="query"
                                placeholder="Search artists, concerts, and cities"
                                className="me-2"
                                aria-label="Search"
                            />
                            <Button type="submit" style={{backgroundColor: "#FE5F55", border: "none"}}>Search</Button>
                            </Form>
                        </Container>

                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default NavBar
