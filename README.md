# MusicMap

### Website Link

https://www.musicmap.me

### Postman

https://documenter.getpostman.com/view/25859804/2s93CEvGRq

### GitLab Pipelines

https://gitlab.com/jaegarciah/cs373-idb-19/-/pipelines


## Group Info

| Name               | GitLabID      | EID     |
|:------------------ | ------------- | ------- |
| Dewayne Benson     | @dtbenson     | dtb2254 |
| Aneesh Chakka      | @aneeshchakka | ac77349 |
| Jae Garcia-Herrera | @jaegarciah   | jdg5266 |
| Amro Kerkiz        | @amr.kerkiz   | ank2268 |
| Jasmine Wang       | @jasmineywang | jw55293 |

### Project Leaders

#### Responsibilities

* Organize and direct group meetings
* Make sure everyone is on track
* Assign issues

| Phase | Project Leader |
|:----- | -------------- |
| 1     | Jasmine Wang   |
| 2     | Jae Garcia-Herrera |
| 3     | Amro Kerkiz    |
| 4     | Aneesh Chakka   |


## Phase 1

### Git SHA
a661583fbfc848328251d8428b990e4f76a47815

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Dewayne Benson     | 20                         | 25                         |
| Aneesh Chakka      | 5                          | 7                          |
| Jae Garcia-Herrera | 10                         | 15                         |
| Amro Kerkiz        | 7                          | 10                         |
| Jasmine Wang       | 20                         | 22                         |

## Phase 2

### Git SHA
e6469a3a695ad2b7aa7a9dcf3801a9d03ee57e65

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Dewayne Benson     | 20                         | 25                         |
| Aneesh Chakka      | 20                         | 25                         |
| Jae Garcia-Herrera | 20                         | 50                         |
| Amro Kerkiz        | 20                         | 50                         |
| Jasmine Wang       | 20                         | 25                         |

## Phase 3

### Git SHA
692abf97172fe3858ec9fb5d664ae29b2718ba61

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Dewayne Benson     | 15                         |  15                        |
| Aneesh Chakka      | 15                         |  15                        |
| Jae Garcia-Herrera | 15                         |  15                        |
| Amro Kerkiz        | 15                         |   15                       |
| Jasmine Wang       | 15                         | 12                         |

## Phase 4

### Git SHA
620fb70af0e334d7dff54cbb59969d0efd2f38bb

### Completion Times

| Name               | Est. Completion Time (hrs) | Real Completion Time (hrs) |
|:------------------ | -------------------------- | -------------------------- |
| Dewayne Benson     | 10                         |  7                        |
| Aneesh Chakka      | 10                         |  5                        |
| Jae Garcia-Herrera | 10                         |  5                        |
| Amro Kerkiz        | 10                         |   5                       |
| Jasmine Wang       | 10                         | 7                         |

## Comments
none
